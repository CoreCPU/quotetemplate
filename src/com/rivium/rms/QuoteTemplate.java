package com.rivium.rms;

import com.rivium.rms.services.TokenService;
import com.rivium.rms.types.TrkdField;
import com.rivium.rms.types.TrkdQuote;
import com.rivium.rms.services.QuoteService;
import com.rivium.rms.services.ServerService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.builder.fluent.Configurations;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class   QuoteTemplate extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.QuoteTemplate");
    final private static TreeMap<String, String> fieldMap = new TreeMap<>();
    final private static TreeMap<String, String> ricMap = new TreeMap<>();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        XMLConfiguration xmlConfig;
        String xmlPassWd = "";
        String xmlAppId = "";
        String xmlUserId = "";
        String xmlRTPassWd = "";
        String xmlRTAppId = "";
        String xmlRTUserId = "";
        String[] xmlConnectAddresses = null;
        String xmlConnectAddress = "";
        String tokenFilePath;
        String realTimeTokenFilePath;
        String passTokenFilePath = "";
        String runConfig = getServletConfig().getInitParameter("ConfigFile");
        String[] fieldsToReturn = null;
        ArrayList<String> delUids = new ArrayList<>();
        ArrayList<String> realUids = new ArrayList<>();
        List<Object> fieldsInConfig;
        List<Object> urlsInConfig;
        List<Object> delayedUids;
        List<Object> realtimeUids;
        LOGGER.info ("\n++++++New QuoteTemplate request++++++\n");
        LOGGER.info ("CLient URL : " + request.getRequestURL());
        LOGGER.info ("Client request : " + request.getQueryString());

        try {
            if (runConfig.isEmpty()) {
                LOGGER.debug("FATAL ERROR : Couldn't read configuration file : " + runConfig);
                LOGGER.info ("No configuration file found.");
                return;
            } else {
                //LOGGER.debug("RunConfig : " + runConfig);
                LOGGER.info("RunConfig : " + runConfig);

                Configurations config = new Configurations();

                try {
                    xmlConfig = config.xml(runConfig);

                    if (xmlConfig.getString("trkd.timelines.delayed").matches("true")) {
                        xmlPassWd = xmlConfig.getString("trkd.delayed.passWd");
                        if (checkString(xmlPassWd)) {
                            LOGGER.info ("Issue with provided delayed configuration values: password");
                            LOGGER.debug("FATAL ERROR : NO DELAYED PASSWD GIVEN");
                            return;
                        }

                        xmlAppId = xmlConfig.getString("trkd.delayed.applicationId");
                        if (checkString(xmlAppId)) {
                            LOGGER.info ("Issue with provided delayed configuration values: applicationId");
                            LOGGER.debug("FATAL ERROR : NO DELAYED APPID GIVEN");
                            return;
                        }

                        xmlUserId = xmlConfig.getString("trkd.delayed.userId");
                        if (checkString(xmlUserId)) {
                            LOGGER.info ("Issue with provided delayed configuration values: uid");
                            LOGGER.debug("FATAL ERROR : NO DELAYED USERID GIVEN");
                            return;
                        }
                    }

                    if (xmlConfig.getString("trkd.timelines.realtime").matches("true")) {
                        xmlRTPassWd = xmlConfig.getString("trkd.realtime.passWd");
                        if (checkString(xmlRTPassWd)) {
                            LOGGER.info ("Issue with provided realtime configuration values: password");
                            LOGGER.debug("FATAL ERROR : NO REALTIME PASSWD GIVEN");
                            return;
                        }

                        xmlRTAppId = xmlConfig.getString("trkd.realtime.applicationId");
                        if (checkString(xmlRTAppId)) {
                            LOGGER.info ("Issue with provided realtime configuration values: applicationId");
                            LOGGER.debug("FATAL ERROR : NO REALTIME APPID GIVEN");
                            return;
                        }

                        xmlRTUserId = xmlConfig.getString("trkd.realtime.userId");
                        if (checkString(xmlRTUserId)) {
                            LOGGER.info ("Issue with provided realtime configuration values: uid");
                            LOGGER.debug("FATAL ERROR : NO REALIME USERID GIVEN");
                            return;
                        }

                        urlsInConfig = xmlConfig.getList( "trkd.connect.servers.server");
                        if (urlsInConfig.isEmpty()) {
                            LOGGER.info ("Issue with provided servers in config");
                            LOGGER.debug("FATAL ERROR : CAN NOT CONNECT");
                            return;
                        } else {
                            xmlConnectAddresses = urlsInConfig.toArray(new String[0]);
                            for (String t : xmlConnectAddresses) {
                                LOGGER.debug("LIST CONTENT : " + t);
                            }
                            ServerService theServer = new ServerService();
                            theServer.setServerList(xmlConnectAddresses);
                            xmlConnectAddress = theServer.getServerName();
                            LOGGER.debug ("THIS IS THE SERVERNAME : " + xmlConnectAddress);
                        }
                    }

                    fieldsInConfig = xmlConfig.getList("fields.quoteFields.field");
                    if (fieldsInConfig.isEmpty()) {
                        LOGGER.debug ("No Quote field list defined.");
                    } else {
                        fieldsToReturn = fieldsInConfig.toArray(new String[0]);
                    }

                    delayedUids = xmlConfig.getList("common.users.delayed.uid");
                    if (delayedUids.isEmpty()) {
                        LOGGER.debug ("No delayed user list defined.");
                        return;
                    } else {
                        //delUids = delayedUids.toArray(new String[delayedUids.size()]);
                        for (Object q : delayedUids){
                            LOGGER.debug("DELAYED User : " + q.toString());
                            String d = q.toString();
                            delUids.add(d);
                        }
                    }

                    realtimeUids = xmlConfig.getList("common.users.realtime.uid");
                    if (realtimeUids.isEmpty()) {
                        LOGGER.debug ("No realtime user list defined.");
                        return;
                    } else {
                        //realUids = realtimeUids.toArray(new String[realtimeUids.size()]);
                        for (Object q : realtimeUids){
                            realUids.add(q.toString());
                            LOGGER.debug("REALTIME USERS: " + q);
                        }
                    }

                    tokenFilePath = xmlConfig.getString("common.tokenfile");

                    if (checkString(tokenFilePath)) {
                        LOGGER.info("No path to delayed tokenfile");
                        LOGGER.debug ("FATAL ERROR : NO TOKENFILE PATH GIVEN");
                        return;
                    }

                    realTimeTokenFilePath = xmlConfig.getString("common.RTtokenfile");

                    if (checkString(realTimeTokenFilePath)) {
                        LOGGER.info("No path to realtime tokenfile");
                        LOGGER.debug ("FATAL ERROR : NO REALTIME TOKENFILE PATH GIVEN");
                        return;
                    }

                    List<Object> xmlMaps = xmlConfig.getList("fields.maps.map.source");

                    if (xmlMaps.isEmpty()) {
                        LOGGER.debug ("No map fields found.");
                    } else {
                        for (int q = 0; q < xmlMaps.size(); q++) {
                            String sourceM = xmlConfig.getString("fields.maps.map(" + q + ").dest");
                            String targetM = xmlConfig.getString("fields.maps.map(" + q + ").source");
                            fieldMap.put(sourceM, targetM);
                            LOGGER.debug("Mapping field [" + targetM + "] to  field : [" + sourceM + "]");
                        }
                    }

                    List<Object> xmlRicMaps = xmlConfig.getList("rics.maps.map.source");
                    if (xmlRicMaps.isEmpty()) {
                        LOGGER.debug ("No map fields defined");
                    } else {
                        for (int q = 0; q < xmlRicMaps.size(); q++) {
                            String sourceRicM = xmlConfig.getString("rics.maps.map(" + q + ").source");
                            String targetRicM = xmlConfig.getString("rics.maps.map(" + q + ").dest");
                            ricMap.put(sourceRicM, targetRicM);
                            LOGGER.debug("Mapping Ric [" + targetRicM + "] to  ric : [" + sourceRicM + "]");
                        }
                    }

                } catch (ConfigurationException ce) {
                    LOGGER.debug ("Configuration Error Message : " + ce.getMessage());
                    return;
                }
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            LOGGER.info ("Configuration file issues. Was it specified in web.xml ?");
            LOGGER.debug ("FATAL ERROR : Configuration not specified in web.xml");
            return;
        }

        String dataRep = request.getParameter("DataRepresentation");
        String AsymBol = request.getParameter("Symbol");
        String userId = request.getParameter("UserId");

        try {
            if (userId.matches("")) {
                userId = "sds";
            }
        } catch (NullPointerException ne) {
            userId = "sds";
        }

        if (delayedUids.contains(userId)) {
            passTokenFilePath = tokenFilePath;
        }

        if (realtimeUids.contains(userId)) {
            passTokenFilePath = realTimeTokenFilePath;
        }

        String symBol = AsymBol.trim();

        try {
            if (symBol.isEmpty()) {
                LOGGER.debug("FATAL ERROR : No symbol given in request string. Unable to fetch data.");
                //System.err.println("FATAL ERROR : No symbol given in request string. Unable to fetch data.");
                return;
            } else {
                LOGGER.debug("DataRepresentation : " + dataRep + " [ ] " + "Symbol : " + symBol + " [ ] " + "UserId : " + userId);
                //System.err.println("DataRepresentation : " + dataRep + "\\t" + "Symbol : " + symBol + "\\t" + "UserId : " + userId);
            }
        } catch (NullPointerException nee) {
            LOGGER.info("Empty symbol.");
            LOGGER.debug ("No value found for Symbol. Likely reason is incorrectly formatted request.");
            //System.err.println("No value found for Symbol. Likely reason is incorrectly formatted request.");
            return;
        }

        LOGGER.info ("Calling TokenService");

        TokenService tokenService = new TokenService();

        if (delUids.contains(userId)) {
            tokenService.setClAppId(xmlAppId);
            tokenService.setClUserId(xmlUserId);
            tokenService.setClPassWd(xmlPassWd);
            LOGGER.debug ("Delayed User Id : " + userId);
            LOGGER.debug ("TRKD DELAYED USER :" + xmlUserId);
        } else if (realUids.contains(userId)) {
            tokenService.setClAppId(xmlRTAppId);
            tokenService.setClUserId(xmlRTUserId);
            tokenService.setClPassWd(xmlRTPassWd);
            LOGGER.debug ("Real time User Id : " + userId);
            LOGGER.debug("TRKD RT-USER : " + xmlRTUserId);
        }

        tokenService.setClTokenFile(passTokenFilePath);
        tokenService.setClUrlConnect(xmlConnectAddress);

        File savedToken = new File(passTokenFilePath);
        String aToken;

        if (savedToken.exists()) {
            aToken = tokenService.GetExistingTokenFromFile();
            if (!aToken.equals("Invalid")) {
                LOGGER.debug("Gotten token from file");
                String validateT = tokenService.ValidateToken(aToken);
                if (validateT.equals("Invalid")) {
                    LOGGER.debug("Token is " + validateT + " Requesting new token.");
                    aToken = tokenService.GetTokenFromCloud();
                    LOGGER.debug("Gotten token from cloud : " + aToken);
                }
            } else {
                LOGGER.debug ("Problem reading token from tokenFile. Requesting new token.");
                aToken = tokenService.GetTokenFromCloud();
                LOGGER.debug("Gotten token from cloud : " + aToken);

            }
        } else {
            aToken = tokenService.GetTokenFromCloud();
            LOGGER.debug("Gotten token from cloud : " + aToken);
        }

        if (aToken.isEmpty()) {
            LOGGER.debug ("No token received from authentication server. Can not complete request.");
            return;
        }

        LOGGER.info ("Calling QuoteService");

        QuoteService quoteService = new QuoteService();

        if(ricMap.containsKey(symBol)) {
            String theMappedRic = ricMap.get(symBol);
            quoteService.setQuoteRequestString(theMappedRic);
        } else {
            quoteService.setQuoteRequestString(symBol);
        }

        if (delUids.contains(userId)) {
            quoteService.setAppId(xmlAppId);
        } else if (realUids.contains(userId)) {
            quoteService.setAppId(xmlRTAppId);
        }

        quoteService.setToken(aToken);
        quoteService.setWantedFieldList(fieldsToReturn);
        quoteService.setConnectAddress(xmlConnectAddress);

        if ((fieldsToReturn != null ? fieldsToReturn.length : 0) > 0) {
            LOGGER.debug ("Selective field list applied.");
        } else {
            LOGGER.debug ("All fields requested.");
        }

        TrkdQuote gottenQuote;

        try {
            gottenQuote = quoteService.getQuote();
            PrintWriter pw = response.getWriter();

            pw.append(makeXml(gottenQuote));

            pw.close();
            LOGGER.info ("Quote request complete.");

        } catch (NullPointerException ne) {
            LOGGER.info ("Quote request failed.");
        }
    }

    private boolean checkString (String checker) {

        boolean retVal = false;

        try {
            if (checker.isEmpty()) {
                LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
                retVal = true;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
        }

        return retVal;
    }

    private String makeXml(TrkdQuote tQ){

        Document doc;
        String resultXml = "";

        //LOGGER.debug("QUOTE TEMPLATE RIC STATUS : " + tQ.getRicStatus() + " for Ric : " + tQ.getRicName());

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            doc = docB.newDocument();
            Element rootEl = doc.createElement("chain");
            doc.appendChild(rootEl);

            if (Objects.equals(tQ.getRicStatus(), "OK")) {
                LOGGER.info("Building XML return for ric " + tQ.getRicName());
                Element ricEl = doc.createElement("ric");
                Attr dataQ = doc.createAttribute("data_quality");
                dataQ.setValue(tQ.getQoS());

                ArrayList<TrkdField> gottenFields = tQ.getFieldList();

                ricEl.setAttributeNode(dataQ);
                rootEl.appendChild(ricEl);

                Element fidEl = doc.createElement("fid");
                Attr fidId = doc.createAttribute("id");
                fidId.setValue("REC_STATUS");
                fidEl.setAttributeNode(fidId);
                fidEl.appendChild(doc.createTextNode("0"));
                ricEl.appendChild(fidEl);

                fidEl = doc.createElement("fid");
                fidId = doc.createAttribute("id");
                fidId.setValue("MSG_TYPE");
                fidEl.setAttributeNode(fidId);
                fidEl.appendChild(doc.createTextNode("0"));
                ricEl.appendChild(fidEl);

                for (TrkdField gottenField : gottenFields) {

                    String fieldVal = gottenField.getFieldValue();

                    if(!fieldVal.isEmpty()) {
                        fidEl = doc.createElement("fid");
                        fidId = doc.createAttribute("id");

                        fidId.setValue(gottenField.getFieldName());
                        fidEl.setAttributeNode(fidId);

                        String checkForDate = gottenField.getFieldName();
                        boolean itIsADate = checkForDate.contains("DAT") || checkForDate.contains("VALUE_DT");

                        if (itIsADate) {
                            String trkdDate = gottenField.getFieldValue();
                            LOGGER.debug("TIME STRING : " + trkdDate + " [ ] " + checkForDate);
                            DateTimeFormatter dtf = DateTimeFormat.forPattern("dd MMM YYYY");
                            try {
                                DateTime jodaTime = dtf.parseDateTime(trkdDate);
                                DateTimeFormatter dtfOut = DateTimeFormat.forPattern("d" + "dMMMyy");
                                String correctedDate = dtfOut.print(jodaTime);
                                fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(correctedDate.toUpperCase())));

                            } catch (IllegalArgumentException ne) {
                                LOGGER.debug("Date string is empty for field " + gottenField.getFieldName() + ". No valued returned.");
                            }
                        } else {
                            String fName = gottenField.getFieldName();
                            //LOGGER.debug("FNAME : " + fName);
                            if (fieldMap.containsKey(fName)) {
                                String mappedValue = tQ.getFieldValueByName(fieldMap.get(fName));
                                //fidEl.appendChild(doc.createTextNode(StringEscapeUtils.escapeXml10(mappedValue)));
                                fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(mappedValue)));
                                //StringEscapeUtils.
                                LOGGER.debug(" Field : " + fName + " mapped to " + gottenField.getFieldName() + " for value [" + mappedValue + "]");
                            } else if ((fName.matches("TRDPRC_1") && (tQ.getFieldValueByName("TRDPRC_1").matches("0.0")))) {
                                LOGGER.debug("MATCH TRDPRC_1");
                                //fidEl.appendChild(doc.createTextNode(StringEscapeUtils.escapeXml10(tQ.getFieldValueByName("HST_CLOSE"))));
                                fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(tQ.getFieldValueByName("HST_CLOSE"))));
                            } else if ((fName.matches("EXCHTIM") && (tQ.getFieldValueByName("EXCHTIM").isEmpty()))) {
                                fidEl.appendChild(doc.createTextNode(" : :"));
                                LOGGER.debug("Mapped EXCHTIM to ' : :'");
                            } else if ((fName.matches("TRDTIM_1") && (tQ.getFieldValueByName("TRDTIM_1").isEmpty()))) {
                                fidEl.appendChild(doc.createTextNode(" :"));
                                LOGGER.debug("Mapped TRDTIM_1 to ' :'");
                            } else {
                                //fidEl.appendChild(doc.createTextNode(StringEscapeUtils.escapeXml10(gottenField.getFieldValue())));
                                fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(gottenField.getFieldValue())));
                                //LOGGER.debug(" Field : " + gottenField.getFieldName() + " [ ] Value : " + gottenField.getFieldValue());
                            }
                        }

                        ricEl.appendChild(fidEl);
                    }
                }

            } else {
                LOGGER.info("FAILURE FOR RIC : " + tQ.getRicName() + " [ ] ERROR : " + tQ.getRicStatus());
                Element ricEl = doc.createElement("ric");
                Attr dataQ = doc.createAttribute("data_quality");
                ricEl.setAttributeNode(dataQ);
                dataQ.setValue("Unknown");
                rootEl.appendChild(ricEl);
                //doc.appendChild(rootEl);

                Element statusEl = doc.createElement("status");
                statusEl.appendChild(doc.createTextNode(tQ.getRicStatus()));
                ricEl.appendChild(statusEl);

                Element infoEl = doc.createElement("info");
                infoEl.appendChild(doc.createTextNode(tQ.getRicName()));
                ricEl.appendChild(infoEl);
            }


            TransformerFactory tFf = TransformerFactory.newInstance();
            Transformer tF = tFf.newTransformer();
            tF.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domS = new DOMSource(doc);
            StreamResult strR = new StreamResult(new StringWriter());

            tF.transform(domS, strR);
            resultXml = strR.getWriter().toString();

        } catch (ParserConfigurationException | TransformerException pe) {
            pe.printStackTrace();
        }

        LOGGER.info ("Xml creation complete for " + tQ.getRicName());
        LOGGER.debug("The Xml back to client : " + resultXml);

        return resultXml;
    }

}