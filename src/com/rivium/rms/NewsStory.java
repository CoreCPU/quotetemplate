package com.rivium.rms;/*
 * Created by alain on 2/25/16.
 */

import com.rivium.rms.services.StoryService;
import com.rivium.rms.services.TokenService;
import com.rivium.rms.services.ServerService;
import com.rivium.rms.types.TrkdNewsItem;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NewsStory extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.NewsStory");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        XMLConfiguration xmlConfig;
        String xmlPassWd = "";
        String xmlAppId = "";
        String xmlUserId = "";
        String xmlRTPassWd = "";
        String xmlRTAppId = "";
        String xmlRTUserId = "";
        String tokenFilePath;
        String realTimeTokenFilePath;
        String passTokenFilePath = "";
        String runConfig = getServletConfig().getInitParameter("ConfigFile");
        String dataRep;
        String StoryId;
        String userId;
        String xmlConnectAddress = "";
        String[] xmlConnectAddresses = null;
        ArrayList<String> delUids = new ArrayList<>();
        ArrayList<String> realUids = new ArrayList<>();
        List<Object> delayedUids;
        List<Object> realtimeUids;
        List<Object> urlsInConfig;

        LOGGER.info("\n++++++New Story request++++++\n");

        request.setCharacterEncoding("UTF-8");
        PrintWriter pw = response.getWriter();
        response.setContentType("text/xml;charset=UTF-8");

        try {
            if (runConfig.isEmpty()) {
                LOGGER.debug("FATAL ERROR : Couldn't read configuration file.");
                LOGGER.info ("No configuration file found.");
                return;
            } else {
                LOGGER.info("RunConfig : " + runConfig);

                Configurations config = new Configurations();

                try {
                    xmlConfig = config.xml(runConfig);

                    if (xmlConfig.getString("trkd.timelines.delayed").matches("true")) {
                        xmlPassWd = xmlConfig.getString("trkd.delayed.passWd");
                        if (checkString(xmlPassWd)) {
                            LOGGER.info ("Issue with provided dalayed configuration values: password");
                            LOGGER.debug("FATAL ERROR : NO DELAYED PASSWD GIVEN");
                            return;
                        }

                        xmlAppId = xmlConfig.getString("trkd.delayed.applicationId");
                        if (checkString(xmlAppId)) {
                            LOGGER.info ("Issue with provided dalayed configuration values: applicationId");
                            LOGGER.debug("FATAL ERROR : NO DELAYED APPID GIVEN");
                            return;
                        }

                        xmlUserId = xmlConfig.getString("trkd.delayed.userId");
                        if (checkString(xmlUserId)) {
                            LOGGER.info ("Issue with provided dalayed configuration values: uid");
                            LOGGER.debug("FATAL ERROR : NO DELAYED USERID GIVEN");
                            return;
                        }
                    }

                    if (xmlConfig.getString("trkd.timelines.realtime").matches("true")) {
                        xmlRTPassWd = xmlConfig.getString("trkd.realtime.passWd");
                        if (checkString(xmlRTPassWd)) {
                            LOGGER.info ("Issue with provided realtime configuration values: password");
                            LOGGER.debug("FATAL ERROR : NO REALTIME PASSWD GIVEN");
                            return;
                        }

                        xmlRTAppId = xmlConfig.getString("trkd.realtime.applicationId");
                        if (checkString(xmlRTAppId)) {
                            LOGGER.info ("Issue with provided realtime configuration values: applicationId");
                            LOGGER.debug("FATAL ERROR : NO REALTIME APPID GIVEN");
                            return;
                        }

                        xmlRTUserId = xmlConfig.getString("trkd.realtime.userId");
                        if (checkString(xmlRTUserId)) {
                            LOGGER.info ("Issue with provided realtime configuration values: uid");
                            LOGGER.debug("FATAL ERROR : NO REALIME USERID GIVEN");
                            return;
                        }

                        urlsInConfig = xmlConfig.getList( "trkd.connect.servers.server");
                        if (urlsInConfig.isEmpty()) {
                            LOGGER.info ("Issue with provided servers in config");
                            LOGGER.debug("FATAL ERROR : CAN NOT CONNECT");
                            return;
                        } else {
                            xmlConnectAddresses = urlsInConfig.toArray(new String[0]);
                            for (String t : xmlConnectAddresses) {
                                LOGGER.debug("LIST CONTENT : " + t);
                            }
                            ServerService theServer = new ServerService();
                            theServer.setServerList(xmlConnectAddresses);
                            xmlConnectAddress = theServer.getServerName();
                            LOGGER.debug ("THIS IS THE SERVERNAME : " + xmlConnectAddress);
                        }
                    }

                    tokenFilePath = xmlConfig.getString("common.tokenfile");

                    if (checkString(tokenFilePath)) {
                        LOGGER.info("No path to delayed tokenfile");
                        LOGGER.debug("FATAL ERROR : NO TOKENFILE PATH GIVEN");
                        return;
                    }

                    realTimeTokenFilePath = xmlConfig.getString("common.RTtokenfile");

                    if (checkString(realTimeTokenFilePath)) {
                        LOGGER.info("No path to realtime tokenfile");
                        LOGGER.debug ("FATAL ERROR : NO REALTIME TOKENFILE PATH GIVEN");
                        return;
                    }

                    delayedUids = xmlConfig.getList("common.users.delayed.uid");
                    if (delayedUids.isEmpty()) {
                        LOGGER.debug ("No delayed user list defined.");
                        return;
                    } else {
                        //delUids = delayedUids.toArray(new String[delayedUids.size()]);
                        for (Object q : delayedUids){
                            String d = q.toString();
                            delUids.add(d);
                        }
                    }

                    realtimeUids = xmlConfig.getList("common.users.realtime.uid");
                    if (realtimeUids.isEmpty()) {
                        LOGGER.debug ("No realtime user list defined.");
                        return;
                    } else {
                        //realUids = realtimeUids.toArray(new String[realtimeUids.size()]);
                        realUids.addAll(realtimeUids.stream().map(Object::toString).collect(Collectors.toList()));
                    }


                } catch (ConfigurationException ce) {
                    LOGGER.debug("Configuration Error Message : " + ce.getMessage());
                    return;
                }
            }
        } catch (NullPointerException ne) {
            LOGGER.info ("Configuration file issues. Was it specified in web.xml ?");
            LOGGER.debug("FATAL ERROR : Configuration not specified in web.xml");
            return;
        }


        dataRep = request.getParameter("DataRepresentation");
        StoryId = request.getParameter("StoryId");
        userId = request.getParameter("userId");

        try {
            if (userId.matches("")) {
                userId = "sds";
            }
        } catch (NullPointerException ne) {
            userId = "sds";
        }

        if (delayedUids.contains(userId)) {
            passTokenFilePath = tokenFilePath;
        }

        if (realtimeUids.contains(userId)) {
            passTokenFilePath = realTimeTokenFilePath;
        }


        try {
            if (StoryId.isEmpty()) {
                LOGGER.info("No query made.");
                LOGGER.debug("FATAL ERROR : No Query String provided. Unable to search for headlines.");
                //System.err.println("FATAL ERROR : No symbol given in request string. Unable to fetch data.");
                return;
            } else {
                LOGGER.debug("DataRepresentation : " + dataRep + "\\t" + "Query String : " + StoryId + "\\t" + "userId : " + userId);
                //System.err.println("DataRepresentation : " + dataRep + "\\t" + "Symbol : " + symBol + "\\t" + "userId : " + userId);
            }
        } catch (NullPointerException nee) {
            LOGGER.info("Story request failed.");
            LOGGER.debug ("No StoryId being provided. Likely reason is incorrectly formatted request.");
            //System.err.println("No value found for Symbol. Likely reason is incorrectly formatted request.");
            return;
        }

        LOGGER.debug ("Calling TokenService");

        TokenService tokenService = new TokenService();

        //if (userId.matches("sds")) {
        if (delUids.contains(userId)) {
            tokenService.setClAppId(xmlAppId);
            tokenService.setClUserId(xmlUserId);
            tokenService.setClPassWd(xmlPassWd);
            passTokenFilePath = tokenFilePath;
            LOGGER.debug ("Delayed User Id : " + userId);
            LOGGER.debug ("DELAYED USER :" + xmlUserId);
        } //else if (userId.matches("sds2"))
        else if (realUids.contains(userId)) {
            tokenService.setClAppId(xmlRTAppId);
            tokenService.setClUserId(xmlRTUserId);
            tokenService.setClPassWd(xmlRTPassWd);
            passTokenFilePath = realTimeTokenFilePath;
            LOGGER.debug ("Real time User Id : " + userId);
            LOGGER.debug("RT-USER : " + xmlRTUserId);
        }

        tokenService.setClTokenFile(passTokenFilePath);
        tokenService.setClUrlConnect(xmlConnectAddress);

        File savedToken = new File(passTokenFilePath);
        String aToken;

        if (savedToken.exists()) {
            aToken = tokenService.GetExistingTokenFromFile();
            if (!aToken.equals("Invalid")) {
                LOGGER.debug("Gotten token from file");
                String validateT = tokenService.ValidateToken(aToken);
                if (validateT.equals("Invalid")) {
                    LOGGER.debug("Token is " + validateT + " Requesting new token.");
                    aToken = tokenService.GetTokenFromCloud();
                    LOGGER.debug("Gotten token from cloud : " + aToken);
                }
            } else {
                LOGGER.debug ("Problem reading token from tokenFile. Requesting new token.");
                aToken = tokenService.GetTokenFromCloud();
                LOGGER.debug("Gotten token from cloud : " + aToken);

            }
        } else {
            aToken = tokenService.GetTokenFromCloud();
            LOGGER.debug("Gotten token from cloud : " + aToken);
        }

        if (aToken.isEmpty()) {
            LOGGER.debug ("No token received from authentication server. Can not complete request.");
            return;
        }

        LOGGER.info ("Calling Story Service.");

        StoryService theStory = new StoryService();

        if (delUids.contains(userId)) {
            theStory.setStAppId(xmlAppId);
        } else if (realUids.contains(userId)) {
            theStory.setStAppId(xmlRTAppId);
        }

        theStory.setStToken(aToken);
        theStory.setStQueryString(StoryId);
        theStory.setConnectAddress(xmlConnectAddress);

        ArrayList<TrkdNewsItem> reqStories = theStory.GetTheStory();

        LOGGER.info("Story request complete.");

        String retStory = makeXml(reqStories);

        LOGGER.debug("Retrieved Story Item. Returned data : " + retStory);

        pw.append(retStory);

        pw.flush();
        pw.close();

        //pw.append(retDoc.toString());
    }

    private boolean checkString (String checker) {

        Boolean retVal = false;

        try {
            if (checker.isEmpty()) {
                LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
                retVal = true;
            } else {
                retVal = false;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
        }

        return retVal;
    }

    private String makeXml(ArrayList<TrkdNewsItem> gottenStories) {

        Document doc;

        String resultXml = "";

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            LOGGER.info ("Building xml for Story Item.");

            doc = docB.newDocument();
            Element NewsML = doc.createElement("NewsML");
            doc.appendChild(NewsML);

            Element Catalog = doc.createElement("Catalog");
            Attr href = doc.createAttribute("Href");
            href.setValue("http://localhost/newsml/catalog-reuters-master_catalog.xml");
            Catalog.setAttributeNode(href);
            NewsML.appendChild(Catalog);

            Element NewsItem = doc.createElement("NewsItem");
            Attr xmlspace = doc.createAttribute("xml:space");
            xmlspace.setValue("preserve");
            NewsItem.setAttributeNode(xmlspace);

            Element Identification = doc.createElement("Identification");

            Element NewsIdentifier = doc.createElement("NewsIdentifier");

            Element ProviderId = doc.createElement("ProviderId");
            ProviderId.setTextContent(gottenStories.get(0).getPR());
            NewsIdentifier.appendChild(ProviderId);

            Element DateId = doc.createElement("DateId");
            DateId.setTextContent("null");
            NewsIdentifier.appendChild(DateId);

            Element NewsItemId = doc.createElement("NewsItemId");
            NewsItemId.setTextContent(gottenStories.get(0).getID());
            NewsIdentifier.appendChild(NewsItemId);

            Element RevisionId = doc.createElement("RevisionId");
            Attr PreviousRevision = doc.createAttribute("PreviousRevision");
            PreviousRevision.setValue("O");

            Attr Update = doc.createAttribute("Update");
            Update.setValue("N");

            RevisionId.setAttributeNode(PreviousRevision);
            RevisionId.setAttributeNode(Update);

            NewsIdentifier.appendChild(RevisionId);

            Element PublicIdentifier = doc.createElement("PublicIdentifier");
            NewsIdentifier.appendChild(PublicIdentifier);

            Identification.appendChild(NewsIdentifier);

            NewsItem.appendChild(Identification);

            NewsML.appendChild(NewsItem);

            Element NewsManagement = doc.createElement("NewsManagement");

            Element NewsItemType = doc.createElement("NewsItemType");
            Attr FormalName = doc.createAttribute("FormalName");
            FormalName.setValue(gottenStories.get(0).getItemType());
            NewsItemType.setAttributeNode(FormalName);
            NewsManagement.appendChild(NewsItemType);

            Element FirstCreated = doc.createElement("FirstCreated");
            FirstCreated.setTextContent(gottenStories.get(0).getCT());
            NewsManagement.appendChild(FirstCreated);

            Element ThisRevisionCreated = doc.createElement("ThisRevisionCreated");
            ThisRevisionCreated.setTextContent(gottenStories.get(0).getRT());
            NewsManagement.appendChild(ThisRevisionCreated);

            Element Status = doc.createElement("Status");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue(gottenStories.get(0).getST());
            Status.setAttributeNode(FormalName);
            NewsManagement.appendChild(Status);

            Status = doc.createElement("Status");
            Attr Urgency = doc.createAttribute("Urgency");
            Urgency.setValue(gottenStories.get(0).getUR());
            Status.setAttributeNode(Urgency);
            NewsManagement.appendChild(Status);

            NewsItem.appendChild(NewsManagement);

            Element NewsComponent = doc.createElement("NewsComponent");
            Attr Duid = doc.createAttribute("Duid");
            Duid.setValue("NC00001");
            Attr EquivalentsList = doc.createAttribute("EquivalentsList");
            EquivalentsList.setValue("no");
            Attr Essential = doc.createAttribute("Essential");
            Essential.setValue("no");
            Attr xmllang = doc.createAttribute("xml:lang");
            xmllang.setValue(gottenStories.get(0).getLN());

            NewsComponent.setAttributeNode(Duid);
            NewsComponent.setAttributeNode(EquivalentsList);
            NewsComponent.setAttributeNode(Essential);
            NewsComponent.setAttributeNode(xmllang);

            Element Role = doc.createElement("Role");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue("MAIN");
            Role.setAttributeNode(FormalName);
            NewsComponent.appendChild(Role);

            Element AdministrativeMetaData = doc.createElement("AdministrativeMetaData");
            Element Provider = doc.createElement("Provider");
            Element Party = doc.createElement("Party");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue("RIS");
            Party.setAttributeNode(FormalName);
            Provider.appendChild(Party);
            AdministrativeMetaData.appendChild(Provider);

            Element Source = doc.createElement("Source");
            Party = doc.createElement("Party");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue(gottenStories.get(0).getAT());
            Party.setAttributeNode(FormalName);
            Source.appendChild(Party);
            AdministrativeMetaData.appendChild(Source);
            NewsComponent.appendChild(AdministrativeMetaData);

            Element NewsComponent1 = doc.createElement("NewsComponent");
            Attr Duid1 = doc.createAttribute("Duid");
            Duid1.setValue("NC00002");
            Attr EquivalentsList1 = doc.createAttribute("EquivalentsList");
            EquivalentsList1.setValue("no");
            Attr Essential1 = doc.createAttribute("Essential");
            Essential1.setValue("no");
            Attr xmllang1 = doc.createAttribute("xml:lang");
            xmllang1.setValue(gottenStories.get(0).getLN());
            NewsComponent1.setAttributeNode(Duid1);
            NewsComponent1.setAttributeNode(EquivalentsList1);
            NewsComponent1.setAttributeNode(Essential1);
            NewsComponent1.setAttributeNode(xmllang1);

            Element Role1 = doc.createElement("Role");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue("MAIN TEXT");
            Role1.setAttributeNode(FormalName);
            NewsComponent1.appendChild(Role1);

            Element NewsLines = doc.createElement("NewsLines");
            Element HeadLine = doc.createElement("HeadLine");
            HeadLine.setTextContent(gottenStories.get(0).getHT());
            NewsLines.appendChild(HeadLine);
            NewsComponent1.appendChild(NewsLines);

            Element ContentItem = doc.createElement("ContentItem");
            Element MediaType = doc.createElement("MediaType");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue("text");
            MediaType.setAttributeNode(FormalName);
            ContentItem.appendChild(MediaType);

            Element Format = doc.createElement("Format");
            FormalName = doc.createAttribute("FormalName");
            FormalName.setValue("XHTML");
            Format.setAttributeNode(FormalName);
            ContentItem.appendChild(Format);

            Element DataContent = doc.createElement("DataContent");
            Element html = doc.createElement("html");
            Element head = doc.createElement("head");
            Element title = doc.createElement("title");
            head.appendChild(title);
            html.appendChild(head);

            Element body = doc.createElement("body");
            body.setTextContent(gottenStories.get(0).getTE());
            html.appendChild(body);

            DataContent.appendChild(html);

            //DataContent.appendChild(body);

            ContentItem.appendChild(DataContent);

            NewsComponent1.appendChild(ContentItem);

            NewsComponent.appendChild(NewsComponent1);

            NewsItem.appendChild(NewsComponent);

            TransformerFactory tFf = TransformerFactory.newInstance();
            Transformer tF = tFf.newTransformer();
            tF.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domS = new DOMSource(doc);
            StreamResult strR = new StreamResult(new StringWriter());

            tF.transform(domS, strR);
            resultXml = strR.getWriter().toString();
            LOGGER.debug ("Story content : " + resultXml);
            LOGGER.info ("Xml creation complete");

        } catch (ParserConfigurationException | TransformerException pe) {
            pe.printStackTrace();
            LOGGER.info ("Xml creation failed for Story Item.");
        }

        return resultXml;
    }
}
