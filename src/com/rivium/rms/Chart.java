package com.rivium.rms;

import com.rivium.rms.services.ServerService;
import com.rivium.rms.services.TimeSeriesService;
import com.rivium.rms.services.TokenService;
import com.rivium.rms.services.ServerService;
import com.rivium.rms.types.TrkdChart;
import com.rivium.rms.types.TrkdQuote;
import com.rivium.rms.services.ChartService;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.builder.fluent.Configurations;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Chart extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.Chart");
    final private static TreeMap<String, String> ricMap = new TreeMap<>();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        XMLConfiguration xmlConfig;
        String xmlPassWd = "";
        String xmlAppId = "";
        String xmlUserId = "";
        String xmlRTPassWd = "";
        String xmlRTAppId = "";
        String xmlRTUserId = "";
        String xmlUrlConnect;
        String tokenFilePath;
        String realTimeTokenFilePath;
        String passTokenFilePath = "";
        String xmlChartFilePath;
        String xmlConnectAddress = "";
        String[] xmlConnectAddresses = null;
        boolean inetOrMpls;
        ArrayList<String> delUids = new ArrayList<>();
        ArrayList<String> realUids;
        List<Object> delayedUids;
        List<Object> realtimeUids;
        List<Object> urlsInConfig;


        TreeMap<String, String> returnedArgsForPane1;
        TreeMap<String, String> returnedArgsForPane2 = new TreeMap<>();

        String runConfig = getServletConfig().getInitParameter("ConfigFile");

        List<Object> fieldsToReturn;

        Configurations config = new Configurations();

        request.setCharacterEncoding("UTF-8");

        PrintWriter pw = response.getWriter();

        LOGGER.info ("\n++++++ New CHART Request ++++++\n");
        LOGGER.info ("CLient URL : " + request.getRequestURL());
        LOGGER.info("Client request : " + request.getQueryString());

        try {
            xmlConfig = config.xml(runConfig);

            if (xmlConfig.getString("trkd.timelines.delayed").matches("true")) {
                xmlPassWd = xmlConfig.getString("trkd.delayed.passWd");
                if (checkString(xmlPassWd)) {
                    LOGGER.debug("FATAL ERROR : NO DELAYED PASSWD GIVEN");
                    return;
                }


                xmlAppId = xmlConfig.getString("trkd.delayed.applicationId");
                if (checkString(xmlAppId)) {
                    LOGGER.debug("FATAL ERROR : NO DELAYED APPID GIVEN");
                    return;
                }

                xmlUserId = xmlConfig.getString("trkd.delayed.userId");
                if (checkString(xmlUserId)) {
                    LOGGER.debug("FATAL ERROR : NO DELAYED USERID GIVEN");
                    return;
                }
            }

            if (xmlConfig.getString("trkd.timelines.realtime").matches("true")) {
                xmlRTPassWd = xmlConfig.getString("trkd.realtime.passWd");
                if (checkString(xmlRTPassWd)) {
                    LOGGER.debug("FATAL ERROR : NO REALTIME PASSWD GIVEN");
                    return;
                }

                xmlRTAppId = xmlConfig.getString("trkd.realtime.applicationId");
                if (checkString(xmlRTAppId)) {
                    LOGGER.debug("FATAL ERROR : NO REALTIME APPID GIVEN");
                    return;
                }

                xmlRTUserId = xmlConfig.getString("trkd.realtime.userId");
                if (checkString(xmlRTUserId)) {
                    LOGGER.debug("FATAL ERROR : NO REALIME USERID GIVEN");
                    return;
                }

                urlsInConfig = xmlConfig.getList( "trkd.connect.servers.server");
                if (urlsInConfig.isEmpty()) {
                    LOGGER.info ("Issue with provided servers in config");
                    LOGGER.debug("FATAL ERROR : CAN NOT CONNECT");
                    return;
                } else {
                    xmlConnectAddresses = urlsInConfig.toArray(new String[0]);
                    for (String t : xmlConnectAddresses) {
                        LOGGER.debug("LIST CONTENT : " + t);
                    }
                    ServerService theServer = new ServerService();
                    theServer.setServerList(xmlConnectAddresses);
                    xmlConnectAddress = theServer.getServerName();
                    LOGGER.debug ("THIS IS THE SERVERNAME : " + xmlConnectAddress);
                }
            }

            tokenFilePath = xmlConfig.getString("common.tokenfile");
            if (checkString(tokenFilePath)) {
                LOGGER.debug("FATAL ERROR : NO TOKENFILE PATH GIVEN.");
                return;
            }

            realTimeTokenFilePath = xmlConfig.getString("common.RTtokenfile");
            if (checkString(realTimeTokenFilePath)) {
                LOGGER.debug("FATAL ERROR : NO REAL TIME TOKENFILE PATH GIVEN.");
                return;
            }

            fieldsToReturn = xmlConfig.getList("fields.timeseriesFields.field");
            if (fieldsToReturn.isEmpty()) {
                LOGGER.debug("No timeseries fields found in configuration.");
            }

            xmlChartFilePath = xmlConfig.getString("common.charts.cacheLocation");
            if (xmlChartFilePath.isEmpty()) {
                LOGGER.debug("No location found for caching charts.");
                return;
            }

            delayedUids = xmlConfig.getList("common.users.delayed.uid");
            if (delayedUids.isEmpty()) {
                LOGGER.debug ("No delayed user list defined.");
                return;
            } else {
                //delUids = delayedUids.toArray(new String[delayedUids.size()]);
                for (Object q : delayedUids){
                    String d = q.toString();
                    delUids.add(d);
                }
            }

            realtimeUids = xmlConfig.getList("common.users.realtime.uid");
            if (realtimeUids.isEmpty()) {
                LOGGER.debug ("No realtime user list defined.");
                return;
            } else {
                //realUids = realtimeUids.toArray(new String[realtimeUids.size()]);
                realUids = new ArrayList<>(realtimeUids.stream().map(Object::toString).collect(Collectors.toList()));
            }

            List<Object> xmlRicMaps = xmlConfig.getList("rics.maps.map.source");
            if (xmlRicMaps.isEmpty()) {
                LOGGER.debug ("No map fields defined");
            } else {
                for (int q = 0; q < xmlRicMaps.size(); q++) {
                    String sourceRicM = xmlConfig.getString("rics.maps.map(" + q + ").source");
                    String targetRicM = xmlConfig.getString("rics.maps.map(" + q + ").dest");
                    ricMap.put(sourceRicM, targetRicM);
                    LOGGER.debug("Mapping Ric [" + sourceRicM + "] to  ric : [" + targetRicM + "]");
                }
            }

        } catch (ConfigurationException ce) {
            LOGGER.debug("Configuration Error Message : " + ce.getMessage());
            return;
        }

        if (runConfig.isEmpty()) {
            LOGGER.debug("FATAL ERROR : Couldn't read configuration file.");
            return;
        } else {
            LOGGER.debug("RunConfig : " + runConfig);
        }

        String dataRep = request.getParameter("DataRepresentation");
        String symBol = request.getParameter("Symbol");
        String userId = request.getParameter("UserId");
        //String chartType = request.getParameter("ChartType");
        String dataType = request.getParameter("DataType");
        //String timeZone = request.getParameter("TimeZone");
        String paneReq = URLDecoder.decode(request.getParameter("Pane1"), "UTF-8");
        String timeP = URLDecoder.decode(request.getParameter("Time"), "UTF-8");
        //String showLegend = request.getParameter("ShowLegend");

        try {
            if (userId.matches("")) {
                userId = "sds";
            }
        } catch (NullPointerException ne) {
            userId = "sds";
        }

        if (delayedUids.contains(userId)) {
            passTokenFilePath = tokenFilePath;
        }

        if (realtimeUids.contains(userId)) {
            passTokenFilePath = realTimeTokenFilePath;
        }

        String paneReq2 = "";

        try {
            paneReq2 = URLDecoder.decode(request.getParameter("Pane2"), "UTF-8");
        } catch (NullPointerException ne) {
            LOGGER.debug("No second pane requested.");
        }

        //String[] splitter = paneReq.split(";");

        TokenService tokenService = new TokenService();

        //if (userId.matches("sds")) {
        if (delUids.contains(userId)) {
            tokenService.setClAppId(xmlAppId);
            tokenService.setClUserId(xmlUserId);
            tokenService.setClPassWd(xmlPassWd);
            LOGGER.debug ("Delayed User Id : " + userId);
            LOGGER.debug ("DELAYED USER :" + xmlUserId);
            passTokenFilePath = tokenFilePath;
        } //else if (userId.matches("sds2"))
        else if (realUids.contains(userId)) {
            tokenService.setClAppId(xmlRTAppId);
            tokenService.setClUserId(xmlRTUserId);
            tokenService.setClPassWd(xmlRTPassWd);
            passTokenFilePath = realTimeTokenFilePath;
            LOGGER.debug ("Real time User Id : " + userId);
            LOGGER.debug("RT-USER : " + xmlRTUserId);
        }

        tokenService.setClTokenFile(passTokenFilePath);
        tokenService.setClUrlConnect(xmlConnectAddress);

        File savedToken = new File(passTokenFilePath);
        String aToken;

        if (savedToken.exists()) {
            aToken = tokenService.GetExistingTokenFromFile();
            if (!aToken.equals("Invalid")) {
                LOGGER.debug("Gotten token from file");
                String validateT = tokenService.ValidateToken(aToken);
                if (validateT.equals("Invalid")) {
                    LOGGER.debug("Token is " + validateT + " Requesting new token.");
                    aToken = tokenService.GetTokenFromCloud();
                    LOGGER.debug("Gotten token from cloud : " + aToken);
                }
            } else {
                LOGGER.debug("Problem reading token from tokenFile. Requesting new token.");
                aToken = tokenService.GetTokenFromCloud();
                LOGGER.debug("Gotten token from cloud : " + aToken);

            }
        } else {
            aToken = tokenService.GetTokenFromCloud();
            LOGGER.debug("Gotten token from cloud : " + aToken);
        }

        if (aToken.isEmpty()) {
            LOGGER.debug("No token received from authentication server. Can not complete request.");
            return;
        }

        try {
            returnedArgsForPane1 = parsePane1Request(paneReq);
        } catch (NullPointerException ne) {
            LOGGER.debug("Request for first pane is incorrect.");
            return;
        }

        try {
            if (!paneReq2.isEmpty()) {
                returnedArgsForPane2 = parsePane2Request(paneReq2);
                LOGGER.debug("PANEREQ2 is Emtpy !");
            }
        } catch (NullPointerException ne) {
            LOGGER.debug("Request for second pane is incorrect or no request made for second pane.");
            paneReq2 = "";
        } catch (ArrayIndexOutOfBoundsException ae) {
            LOGGER.debug("Second pane was not requested. ");
            ae.printStackTrace();
            paneReq2 = "";
            //returnedArgsForPane1.put("Volume", "false");
        }

        DateTime stopTime = new DateTime();
        DateTimeFormatter dfm = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

        String[] splitTime = timeP.split(" ");

        DateTime startTime = null;

        //System.err.println("SPLIT TIME : " + timeP);

        if (splitTime[1].contains("Month")) {
            startTime = stopTime.minusMonths(Integer.parseInt(splitTime[0]));
        } else if (splitTime[1].contains("Year")) {
            startTime = stopTime.minusYears(Integer.parseInt(splitTime[0]));
        }


        if (Objects.equals(dataRep, "CSV")) {

            TimeSeriesService tService = new TimeSeriesService();

            if (delUids.contains(userId)) {
                tService.setApplicationId(xmlAppId);
            } else if (realUids.contains(userId)) {
                tService.setApplicationId(xmlRTAppId);
            }

            tService.setInterValPeriod(dataType);

            tService.setStopTime(stopTime.toString(dfm));

            tService.setStartTime(startTime != null ? startTime.toString(dfm) : null);

            String theChartSymbol = returnedArgsForPane1.get("Ric");

            tService.setConnectAddress(xmlConnectAddress);

            if(ricMap.containsKey(theChartSymbol)) {
                String theMappedRic = ricMap.get(theChartSymbol);
                tService.setsymBol(theMappedRic);
            } else {
                tService.setsymBol(theChartSymbol);
            }


            tService.setToken(aToken);

            LOGGER.info("Calling TimeSeriesService for ric " + returnedArgsForPane1.get("Ric"));

            ArrayList<TrkdQuote> timeSeriesRows = tService.getTimeSeries();

            LOGGER.debug("Size of returned timeSeries : " + timeSeriesRows.size());

            if (timeSeriesRows.isEmpty() || (timeSeriesRows.size() == 0)) {
                response.setContentType("application/txt;charset=UTF-8");
                pw.append(" /images/err.png");
                pw.flush();
                pw.close();
                LOGGER.debug("Error requesting timeSeries for ric " + returnedArgsForPane1.get("Ric"));
            } else {
                StringBuilder sendString = new StringBuilder();
                sendString.append("\n");
                sendString.append("Symbol:");
                sendString.append(returnedArgsForPane1.get("Ric"));
                sendString.append(",Kind:");
                sendString.append(returnedArgsForPane1.get("Title"));
                sendString.append("\nDate,Open,High,Low,Close\n");


                for (TrkdQuote tQ : timeSeriesRows) {
                    String formTheDate = formDate(tQ.getFieldValueByName("TIMESTAMP"));
                    sendString.append(formTheDate);
                    sendString.append(",");
                    sendString.append(tQ.getFieldValueByName("OPEN"));
                    sendString.append(",");
                    sendString.append(tQ.getFieldValueByName("HIGH"));
                    sendString.append(",");
                    sendString.append(tQ.getFieldValueByName("LOW"));
                    sendString.append(",");
                    sendString.append(tQ.getFieldValueByName("CLOSE"));
                    sendString.append("\n");
                    LOGGER.debug("Row : " + tQ.getRicName());
                }

                if (returnedArgsForPane1.containsKey("SecPane")) {
                    sendString.append("\n");
                    sendString.append("Symbol:");
                    sendString.append(returnedArgsForPane1.get("Ric"));
                    sendString.append(",Kind:");
                    sendString.append(returnedArgsForPane1.get("SecPane"));
                    sendString.append("\nDate,Volume\n");

                    for (TrkdQuote tQ : timeSeriesRows) {

                        String formTheDate = formDate(tQ.getFieldValueByName("TIMESTAMP"));
                        sendString.append(formTheDate);
                        sendString.append(",");
                        sendString.append(tQ.getFieldValueByName("VOLUME"));
                        sendString.append("\n");
                    }
                }

                if (!paneReq2.isEmpty()) {

                    sendString.append("\nSymbol:");
                    sendString.append(returnedArgsForPane2.get("Ric"));
                    sendString.append(",Kind:");
                    sendString.append(returnedArgsForPane2.get("Title"));
                    sendString.append("\n");
                    sendString.append("Date,");
                    sendString.append(returnedArgsForPane2.get("Title"));
                    sendString.append("\n");

                    //String dumTest1 = "";
                    for (TrkdQuote tQ : timeSeriesRows) {
                        //sendString.append(tQ.getFieldValueByName("TIMESTAMP"));
                        String formTheDate = formDate(tQ.getFieldValueByName("TIMESTAMP"));
                        sendString.append(formTheDate);
                        sendString.append(",");
                        sendString.append(tQ.getFieldValueByName("VOLUME"));
                        sendString.append("\n");
                    }


                }

                LOGGER.debug ("SENDSTRING : " + sendString);

                //byte[] toSend = sendString.toString().getBytes(Charset.forName("UTF-8"));

                response.setContentType(("application/txt"));
                response.setHeader("Content-Disposition", "attachement; filename=\"Chart(1)\"");
                //response.setContentLength((int) toSend.length);
                response.setContentLength(sendString.length());
                pw.append(sendString);
                pw.flush();
                pw.close();
                LOGGER.info("Sent timeSeries for ric " + symBol + " to client");
            }
         //end of CSV
        } else if (Objects.equals(dataRep, "Charts")) {
            LOGGER.info ("Chart requested, preparing for pick-up.");
            ChartService theChart = new ChartService();
            theChart.setToken(aToken);
            theChart.setConnectAddress(xmlConnectAddress);

            if (delUids.contains(userId)) {
                theChart.setApplicationID(xmlAppId);
            } else if (realUids.contains(userId)) {
                theChart.setApplicationID(xmlRTAppId);
            }

            String theChartSymbol = returnedArgsForPane1.get("Ric");

            if(ricMap.containsKey(theChartSymbol)) {
                String theMappedRic = ricMap.get(theChartSymbol);
                theChart.setRic(theMappedRic);
            } else {
                theChart.setRic(theChartSymbol);
            }

            //theChart.setRic(returnedArgsForPane1.get("Ric"));

            theChart.setStartDate(startTime != null ? startTime.toString(dfm) : null);
            theChart.setStopDate(stopTime.toString(dfm));
            theChart.setVolumeYN(returnedArgsForPane1.get("Volume"));
            theChart.setAnalysis(returnedArgsForPane1.get("Analysis"));
            LOGGER.debug ("VOLUME : " + returnedArgsForPane1.get("Volume"));
            if (!returnedArgsForPane2.isEmpty()) {
                theChart.setVolumeYN(returnedArgsForPane2.get("Volume"));
                LOGGER.debug ("VOLUME : " + returnedArgsForPane2.get("Volume"));
            }

            TrkdChart chartData = theChart.getChart();
            String savedChart = pickUpChart(chartData, xmlChartFilePath);
            PrintWriter retW = response.getWriter();
            retW.write(savedChart);
            retW.flush();
            retW.close();
            LOGGER.info ("Chart picked up and stored on disk.");
        }
    }

    private boolean checkString (String checker) {

        boolean retVal = false;

        try {
            if (checker.isEmpty()) {
                LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
                retVal = true;
            } else {
                retVal = false;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
        }

        return retVal;
    }

    private TreeMap<String, String> parsePane1Request(String paneRequest) {

        //ArrayList<String> pane1Args = new ArrayList<>();
        TreeMap<String, String> pane1Args = new TreeMap<>();

        pane1Args.put("Volume", "false");

        //100;(Price,GSZ.PA,OHLC,OHLC,blue);(Volume,GSZ.PA,red)
        String[] pane1Parts = paneRequest.split(";");

        //Pane1=100;(Price,RIO.L,OHLC,OHLC,blue);(Volume,RIO.L,red)

        try {
            String dummy = removeBrackets(pane1Parts[1]);
            String[] theArgs = dummy.split(",");
            for (String theArg : theArgs) {
                System.err.println(theArg);
            }

            pane1Args.put("Title", theArgs[0]);

            if (theArgs[1].contains("|")) {
                String[] splitName = theArgs[1].split("\\|");
                pane1Args.put("Ric", splitName[0]);
            } else {
                pane1Args.put("Ric", theArgs[1]);
            }

            pane1Args.put("Fields", theArgs[2]);
            pane1Args.put("Analysis", theArgs[3]);
            pane1Args.put("Color", theArgs[4]);

            try {
                LOGGER.debug("PART1VOL : " + pane1Parts[2]);

                if (!pane1Parts[2].isEmpty()) {
                    dummy = removeBrackets(pane1Parts[2]);
                    theArgs = dummy.split(",");
                    pane1Args.put("SecPane", theArgs[0]);
                    pane1Args.put("SecPaneRic", theArgs[1]);
                    pane1Args.put("SecPaneColor", theArgs[2]);
                    pane1Args.put("Volume", "true");
                    LOGGER.debug ("Sec Part: " + pane1Parts[2]);

                } else {
                    LOGGER.debug ("No volume part found in pane1 request.");
                    pane1Args.put("Volume", "false");
                }
            } catch (ArrayIndexOutOfBoundsException ae) {
                LOGGER.debug ("No volume request found in pane1 request");
                pane1Args.put("Volume", "false");
            }
        } catch (NullPointerException ne){
            LOGGER.debug ("Error while parsing pane1 arguments");
        }

        return pane1Args;
    }

    private TreeMap<String, String> parsePane2Request(String paneRequest){

        TreeMap<String, String> pane2Args = new TreeMap<>();

        String[] pane2Parts = paneRequest.split(";");

        //(Volume,RIO.L,red)

        LOGGER.debug ("PANE2 REQ : " + paneRequest);
        //LOGGER.debug("PART1_2 : " + pane2Parts[0]);

        String dummy = removeBrackets(pane2Parts[1]);
        String[] theArgs = dummy.split(",");
        pane2Args.put("Title", theArgs[0]);
        pane2Args.put("Ric", theArgs[1]);
        pane2Args.put("Color", theArgs[2]);
        pane2Args.put("Volume", "true");

        return pane2Args;
    }

    private String removeBrackets(String inputStr) {
        String noBrackets;
        String dummy;

        dummy = inputStr.replace("(", "");
        noBrackets = dummy.replace(")", "");

        return noBrackets;
    }

    private String formDate (String dateTime){

        String aDate = "";
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern);

        //String cDate = dateTime.replace("+00:00", "");

        String dateY = dateTime.substring(0, 10);
        //String[] dateParts = dateY.split("-");

        //String dateT = dateTime.substring(11);

        try {
            aDate = dateFormatter.parse(dateY).toString();

        } catch (ParseException ne) {
            ne.printStackTrace();

        }
        return aDate;
    }

    private String pickUpChart(TrkdChart theChart, String cachePath){

        String theLocation;
        String saveChartPath;
        String returnPath;

        BufferedImage trkdChart;

        String imageTag = theChart.getImageTag();

        if (cachePath.startsWith("/")) {
            saveChartPath = cachePath;
            String[] splitPath = cachePath.split("/");
            returnPath = splitPath[splitPath.length];
        } else {
            saveChartPath = getServletContext().getRealPath(cachePath);
            returnPath = cachePath;
            //returnPath = saveChartPath;
        }

        StringBuilder pathToSave = new StringBuilder();
        pathToSave.append(saveChartPath);
        pathToSave.append("/");
        pathToSave.append(imageTag);
        pathToSave.append(".png");

        theLocation = theChart.getHttpUrl();

        try {
            URL chartUrl = new URL(theLocation);
            trkdChart = ImageIO.read(chartUrl);
            ImageIO.write(trkdChart, "png", new File(pathToSave.toString()));
        } catch (MalformedURLException me){
            me.printStackTrace();
            LOGGER.debug("Chart url incorrectly formatted");
        } catch (IOException ie) {
            ie.printStackTrace();
            LOGGER.debug ("Issues with saving retrieved chart.");
        }

        StringBuilder pathToReturn = new StringBuilder();
        pathToReturn.append("/servlet/");
        pathToReturn.append(returnPath);
        pathToReturn.append("/");
        pathToReturn.append(imageTag);
        pathToReturn.append(".png");

        LOGGER.info ("Chart Path sent to browser : " + pathToReturn);

        return pathToReturn.toString();
    }
}