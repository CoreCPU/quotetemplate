package com.rivium.rms;

/*
 * Created by alain on 1/28/16.
 */

import com.rivium.rms.services.WatchListService;
import com.rivium.rms.services.TokenService;
import com.rivium.rms.services.ServerService;
import com.rivium.rms.types.TrkdField;
import com.rivium.rms.types.TrkdQuote;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Service;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

public class Watchlist extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.Watchlist");
    final private static TreeMap<String, String> fieldMap = new TreeMap<>();
    private String QOS;

    final private static TreeMap<String, String> ricMap = new TreeMap<>();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        XMLConfiguration xmlConfig;
        String xmlPassWd = "";
        String xmlAppId = "";
        String xmlUserId = "";
        String xmlRTPassWd = "";
        String xmlRTAppId = "";
        String xmlRTUserId = "";
        String[] xmlConnectAddresses = null;
        String xmlConnectAddress = "";
        String tokenFilePath;
        String realTimeTokenFilePath;
        String passTokenFilePath = "";
        String runConfig = getServletConfig().getInitParameter("ConfigFile");
        ArrayList<String> delUids = new ArrayList<>();
        ArrayList<String> realUids = new ArrayList<>();
        List<Object> delayedUids;
        List<Object> realtimeUids;
        List<Object> urlsInConfig;

        TreeMap<String, String> exchangeProps = new TreeMap<>();

        List<Object> exchanges;

        ArrayList<TrkdQuote> returnedValues;

        List<String> symbolVals;
        List<String> fieldVals;

        request.setCharacterEncoding("UTF-8");

        LOGGER.info ("\n++++++ New WATCHLIST Request++++++\n");
        LOGGER.debug ("CLient URL : " + request.getRequestURL());
        LOGGER.info ("Client request : " + request.getQueryString());

        if (runConfig.isEmpty()) {
            LOGGER.debug("FATAL ERROR : Couldn't read configuration file.");
            return;
        } else {
            LOGGER.info("RunConfig : " + runConfig);
        }

        //String dataRep = request.getParameter("DataRepresentation");
        String symBolList = request.getParameter("SymbolList");
        LOGGER.info ("Requested Symbol List : " + symBolList);
        String fieldList = request.getParameter("FieldList");
        String userId = request.getParameter("UserId");

        try {
            if (userId.matches("")) {
                userId = "sds";
            }
        } catch (NullPointerException ne) {
            userId = "sds";
        }

        if (userId.matches("sds")) {
            QOS = "DELAYED";
        } else {
            QOS = "REALTIME";
        }

        Configurations config = new Configurations();

        try {
            xmlConfig = config.xml(runConfig);

            if (xmlConfig.getString("trkd.timelines.delayed").matches("true")) {
                xmlPassWd = xmlConfig.getString("trkd.delayed.passWd");
                if (checkString(xmlPassWd)) {
                    LOGGER.info ("Issue with provided dalayed configuration values: password");
                    LOGGER.debug("FATAL ERROR : NO DELAYED PASSWD GIVEN");
                    return;
                }


                xmlAppId = xmlConfig.getString("trkd.delayed.applicationId");
                if (checkString(xmlAppId)) {
                    LOGGER.info ("Issue with provided dalayed configuration values: applicationId");
                    LOGGER.debug("FATAL ERROR : NO DELAYED APPID GIVEN");
                    return;
                }

                xmlUserId = xmlConfig.getString("trkd.delayed.userId");
                if (checkString(xmlUserId)) {
                    LOGGER.info ("Issue with provided dalayed configuration values: uid");
                    LOGGER.debug("FATAL ERROR : NO DELAYED USERID GIVEN");
                    return;
                }
            }

            if (xmlConfig.getString("trkd.timelines.realtime").matches("true")) {
                xmlRTPassWd = xmlConfig.getString("trkd.realtime.passWd");
                if (checkString(xmlRTPassWd)) {
                    LOGGER.info ("Issue with provided realtime configuration values: password");
                    LOGGER.debug("FATAL ERROR : NO REALTIME PASSWD GIVEN");
                    return;
                }

                xmlRTAppId = xmlConfig.getString("trkd.realtime.applicationId");
                if (checkString(xmlRTAppId)) {
                    LOGGER.info ("Issue with provided realtime configuration values: applicationID");
                    LOGGER.debug("FATAL ERROR : NO REALTIME APPID GIVEN");
                    return;
                }

                xmlRTUserId = xmlConfig.getString("trkd.realtime.userId");
                if (checkString(xmlRTUserId)) {
                    LOGGER.info ("Issue with provided realtime configuration values: uid");
                    LOGGER.debug("FATAL ERROR : NO REALIME USERID GIVEN");
                    return;
                }

                urlsInConfig = xmlConfig.getList( "trkd.connect.servers.server");
                if (urlsInConfig.isEmpty()) {
                    LOGGER.info ("Issue with provided servers in config");
                    LOGGER.debug("FATAL ERROR : CAN NOT CONNECT");
                    return;
                } else {
                    xmlConnectAddresses = urlsInConfig.toArray(new String[0]);
                    for (String t : xmlConnectAddresses) {
                        LOGGER.debug("LIST CONTENT : " + t);
                    }
                    ServerService theServer = new ServerService();
                    theServer.setServerList(xmlConnectAddresses);
                    xmlConnectAddress = theServer.getServerName();
                    LOGGER.debug ("THIS IS THE SERVERNAME : " + xmlConnectAddress);
                }
            }

            tokenFilePath = xmlConfig.getString("common.tokenfile");
            if (checkString(tokenFilePath)) {
                LOGGER.info("No path to delayed tokenfile");
                LOGGER.debug("FATAL ERROR : NO TOKENFILE PATH GIVEN.");
                return;
            }

            realTimeTokenFilePath = xmlConfig.getString("common.RTtokenfile");
            if (checkString(realTimeTokenFilePath)) {
                LOGGER.info("No path to realtime tokenfile");
                LOGGER.debug ("FATAL ERROR : NO REALTIME TOKENFILE PATH GIVEN");
                return;
            }

            exchanges = xmlConfig.getList("exchanges.exchange.name");
            if (exchanges.isEmpty()) {
                LOGGER.debug("No exchanges defined in config file.");
            } else {
                for (int q = 0; q < exchanges.size(); q++) {
                    String exchangeName = xmlConfig.getString("exchanges.exchange(" + q + ").name");
                    String exchangeItem = xmlConfig.getString("exchanges.exchange(" + q + ").startitem");
                    exchangeProps.put(exchangeName, exchangeItem);
                }
            }

            delayedUids = xmlConfig.getList("common.users.delayed.uid");
            if (delayedUids.isEmpty()) {
                LOGGER.debug ("No delayed user list defined.");
                return;
            } else {
                //delUids = delayedUids.toArray(new String[delayedUids.size()]);
                for (Object q : delayedUids){
                    String d = q.toString();
                    delUids.add(d);
                }
            }

            realtimeUids = xmlConfig.getList("common.users.realtime.uid");
            if (realtimeUids.isEmpty()) {
                LOGGER.debug ("No realtime user list defined.");
                return;
            } else {
                //realUids = realtimeUids.toArray(new String[realtimeUids.size()]);
                realUids.addAll(realtimeUids.stream().map(Object::toString).collect(Collectors.toList()));
            }

            List<Object> xmlMaps = xmlConfig.getList("fields.maps.map.source");
            if (xmlMaps.isEmpty()) {
                LOGGER.debug ("No map fields defined");
            } else {
                for (int q = 0; q < xmlMaps.size(); q++) {
                    String sourceM = xmlConfig.getString("fields.maps.map(" + q + ").dest");
                    String targetM = xmlConfig.getString("fields.maps.map(" + q + ").source");
                    fieldMap.put(sourceM, targetM);
                    LOGGER.debug("Mapping field [" + sourceM + "] to  field : [" + targetM + "]");
                }
            }

            List<Object> xmlRicMaps = xmlConfig.getList("rics.maps.map.source");

            if (xmlRicMaps.isEmpty()) {
                LOGGER.debug ("No map fields defined");
            } else {
                for (int q = 0; q < xmlRicMaps.size(); q++) {
                    String sourceRicM = xmlConfig.getString("rics.maps.map(" + q + ").source");
                    String targetRicM = xmlConfig.getString("rics.maps.map(" + q + ").dest");
                    ricMap.put(sourceRicM, targetRicM);
                    LOGGER.debug("Mapping Ric [" + sourceRicM + "] to  ric : [" + targetRicM + "]");
                }
            }

        } catch (ConfigurationException ce) {
            LOGGER.debug("Configuration Error Message : " + ce.getMessage());
            LOGGER.info ("Configuration file issues. Was it specified in web.xml ?");
            return;
        }

        LOGGER.debug("Calling Token Service");

        TokenService tokenService = new TokenService();

        //if (userId.matches("sds")) {
        if (delUids.contains(userId)) {
            tokenService.setClAppId(xmlAppId);
            tokenService.setClUserId(xmlUserId);
            tokenService.setClPassWd(xmlPassWd);
            passTokenFilePath = tokenFilePath;
            LOGGER.debug ("Request User Id : " + userId);
            LOGGER.debug ("DELAYED USER :" + xmlUserId);
        } //else if (userId.matches("sds2"))
        else if (realUids.contains(userId)) {
            tokenService.setClAppId(xmlRTAppId);
            tokenService.setClUserId(xmlRTUserId);
            tokenService.setClPassWd(xmlRTPassWd);
            passTokenFilePath = realTimeTokenFilePath;
            LOGGER.debug ("Request User Id : " + userId);
            LOGGER.debug("RT-USER : " + xmlRTUserId);
        }

        tokenService.setClTokenFile(passTokenFilePath);
        tokenService.setClUrlConnect(xmlConnectAddress);

        File savedToken = new File(passTokenFilePath);
        String aToken;

        if (savedToken.exists()) {
            aToken = tokenService.GetExistingTokenFromFile();
            LOGGER.debug("Gotten existing token from file");
            if (!aToken.equals("Invalid")) {
                String validateT = tokenService.ValidateToken(aToken);
                LOGGER.debug ("Checking validity of existing token");
                if (validateT.equals("Invalid")) {
                    LOGGER.debug("Token is " + validateT + ". Requesting new token from cloud.");
                    aToken = tokenService.GetTokenFromCloud();
                    LOGGER.debug("Gotten token from cloud : " + aToken);
                }
            } else {
                LOGGER.debug("Problem reading token from tokenFile. Requesting new token from cloud.");
                aToken = tokenService.GetTokenFromCloud();
                LOGGER.debug("Gotten token from cloud : " + aToken);

            }
        } else {
            LOGGER.debug("No token file found. Requesting new token from cloud.");
            aToken = tokenService.GetTokenFromCloud();
            LOGGER.debug("Gotten token from cloud : " + aToken);
        }

        if (aToken.isEmpty()) {
            LOGGER.debug("No token received from authentication server. Can not complete request.");
            return;
        }

        try {
            //TODO: make sure there is an actual list, not simply one item
            //symbolVals = symBolList.split(";");
            symbolVals = Arrays.asList(symBolList.split(";"));
            LOGGER.debug("SYMBOL LIST :" + symbolVals.toString());
        } catch (NullPointerException ne) {
            LOGGER.debug("Issue with supplied symbolList : " + symBolList);
            return;
        }

        try {
            String fList = fieldList +
                    ";" +
                    "CF_NAME;CF_BID;CF_ASK";
            //fieldVals = fieldList.split(";");
            fieldVals = Arrays.asList(fList.split(";"));
        } catch (NullPointerException ne) {
            LOGGER.debug("Issue with supplied fieldList : " + fieldList);
            return;
        }

        PrintWriter pw = response.getWriter();
        response.setContentType("text/xml;charset=UTF-8");

        WatchListService qList = new WatchListService();

        qList.setQuoteList(symbolVals);

        if (delUids.contains(userId)) {
            qList.setAppId(xmlAppId);
        } else if (realUids.contains(userId)) {
            qList.setAppId(xmlRTAppId);
        }

        qList.setToken(aToken);
        qList.setTheProps(exchangeProps);
        qList.setRequestedFieldList(fieldVals);
        qList.setConnectAddress(xmlConnectAddress);

        LOGGER.info("Connecting to " + xmlConnectAddress);

        LOGGER.info("Calling WatchList Service");

        returnedValues = qList.getList();

        LOGGER.info("RETURNED LIST :", returnedValues);

        LOGGER.info("WatchList Service called.");

        if (returnedValues.isEmpty()) {
            //response.setContentType("text/xml;charset=UTF-8");
            pw.append("<chain>");
            pw.append("<ric data_quality=\"Unknown\">");
            pw.append("<status>Unknown item in request.</status>");
            pw.append("<info/>");
            pw.append("</ric>");
            pw.append("</chain>");
            pw.flush();
            pw.close();
            LOGGER.info ("No watchlist values returned.");
            LOGGER.debug("Watchlist result is empty for request : " + (symbolVals.toString()));
        } else {
            LOGGER.info("Number of items returned : " + returnedValues.size());
            LOGGER.debug ("Building xml response for watchlist.");
            String someDoc = makeXml(returnedValues);
            pw.append(someDoc);
            pw.flush();
            pw.close();

            for (TrkdQuote sQuote : returnedValues) {
                LOGGER.debug("LIST MEMBER : " + sQuote.getRicName());
            }
        }
    }

    private boolean checkString (String checker) {

        Boolean retVal = false;

        try {
            if (checker.isEmpty()) {
                LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
                retVal = true;
            } else {
                retVal = false;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
        }

        return retVal;
    }

    private String makeXml (ArrayList<TrkdQuote> incomingQs) {

        Document doc;
        String resultXml = "";

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();

            doc = docB.newDocument();
            doc.setXmlStandalone(true);
            Element rootEl = doc.createElement("watchlist");
            doc.appendChild(rootEl);
            //LOGGER.debug ("DOC CREATED< ELEMENT ADDED");

            if (!incomingQs.isEmpty()){

                for (TrkdQuote tQ : incomingQs) {

                    Element ricEl = doc.createElement("ric");
                    Attr dataQ = doc.createAttribute("data_quality");

                    LOGGER.info ("Constructing Xml : " + tQ.getRicName() + " [ ] " + tQ.getRicStatus() + " [ ] " + tQ.getRicType());

                    if ((tQ.getRicStatus().matches("OK"))) {
                        LOGGER.debug(": " + tQ.getRicName());
                        ArrayList<TrkdField> gottenFields = tQ.getFieldList();
                        //dataQ.setValue(tQ.getQoS());
                        dataQ.setValue(QOS);
                        ricEl.setAttributeNode(dataQ);

                        Element fidEl = doc.createElement("fid");
                        Attr fidId = doc.createAttribute("id");
                        fidId.setValue("REC_STATUS");
                        fidEl.setAttributeNode(fidId);
                        fidEl.appendChild(doc.createTextNode("0"));
                        ricEl.appendChild(fidEl);

                        fidEl = doc.createElement("fid");
                        fidId = doc.createAttribute("id");
                        fidId.setValue("MSG_TYPE");
                        fidEl.setAttributeNode(fidId);
                        fidEl.appendChild(doc.createTextNode("0"));
                        ricEl.appendChild(fidEl);

                        for (TrkdField gottenField : gottenFields) {
                            fidEl = doc.createElement("fid");
                            fidId = doc.createAttribute("id");

                            try {

                                fidId.setValue(gottenField.getFieldName());
                                fidEl.setAttributeNode(fidId);

                                String checkForDate = gottenField.getFieldName();
                                Boolean itIsADate = checkForDate.contains("DAT");

                                if (itIsADate) {
                                    String trkdDate = gottenField.getFieldValue();
                                    DateTimeFormatter dtf = DateTimeFormat.forPattern("dd MMM YYYY");
                                    try {
                                        DateTime jodaTime = dtf.parseDateTime(trkdDate);
                                        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("d" + "dMMMyy");
                                        String correctedDate = dtfOut.print(jodaTime);
                                        fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(correctedDate.toUpperCase())));
                                        ricEl.appendChild(fidEl);
                                    } catch (IllegalArgumentException ne) {
                                        ne.printStackTrace();
                                    }
                                } else {
                                    String fName = gottenField.getFieldName();
                                    LOGGER.debug("FNAME : " + fName);
                                    if (fieldMap.containsKey(fName)){
                                        String mappedValue = tQ.getFieldValueByName(fieldMap.get(fName));
                                        fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(mappedValue)));
                                        LOGGER.debug(" Field : " + fName + " mapped to " + gottenField.getFieldName() + " for value [" + mappedValue + "]");
                                    } else if ((fName.matches("TRDPRC_1") && (tQ.getFieldValueByName("TRDPRC_1").matches("0")))) {
                                        LOGGER.debug ("TRDPRC_1 all values zero: Mapped to HST_CLOSE.");
                                        fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(tQ.getFieldValueByName("HST_CLOSE"))));
                                    } else {
                                        fidEl.appendChild(doc.createTextNode(StringEscapeUtils.unescapeXml(gottenField.getFieldValue())));
                                    }

                                    ricEl.appendChild(fidEl);
                                }
                            } catch (NullPointerException ignored) {

                            }

                            LOGGER.debug("Field Members : " + gottenField.getFieldName() + " [ ] " + gottenField.getFieldValue());
                        }

                        fidEl = doc.createElement("fid");
                        fidId = doc.createAttribute("id");
                        fidId.setValue("SYMBOL");
                        Attr fidUrl = doc.createAttribute("url");
                        String str = "Watchlist.jsp?SymbolList=" + tQ.getRicName();
                        //fidUrl.setValue("Watchlist.jsp?SymbolList=");
                        fidUrl.setValue(str);
                        fidEl.setAttributeNode(fidId);
                        fidEl.setAttributeNode(fidUrl);
                        fidEl.appendChild(doc.createTextNode(tQ.getRicName()));
                        ricEl.appendChild(fidEl);
                        LOGGER.debug("Processing done for : " + tQ.getRicName());
                    } else {
                        //Shouldnt get here as empty list is handled in doPost
                        LOGGER.info("FAILURE FOR RIC : " + tQ.getRicName() + " [ ] ERROR : " + tQ.getRicStatus());
                        dataQ = doc.createAttribute("data_quality");
                        dataQ.setValue(tQ.getRicStatus());
                        ricEl.setAttributeNode(dataQ);
                        rootEl.appendChild(ricEl);

                        Element statusEl = doc.createElement("status");
                        statusEl.appendChild(doc.createTextNode(tQ.getRicStatus()));
                        ricEl.appendChild(statusEl);

                        Element infoEl = doc.createElement("info");
                        infoEl.appendChild(doc.createTextNode(tQ.getRicName()));
                        ricEl.appendChild(infoEl);
                    }

                    rootEl.appendChild(ricEl);
                }

                //LOGGER.debug ("THE DATA : " + doc.toString());

            } else {
                //TODO: better error when watchlist fails because of unknown member
                Element errorEl = doc.createElement("error");
                rootEl.appendChild(errorEl);
                Element infoEl = doc.createElement("info");
                infoEl.appendChild(doc.createTextNode("Watchlist fetch failed. Unknown item in list."));
                errorEl.appendChild(infoEl);
                LOGGER.debug ("CATASTROPHY: Watchlist fetch failed. Unknown item in list.");
            }

            TransformerFactory tFf = TransformerFactory.newInstance();
            Transformer tF = tFf.newTransformer();
            tF.setOutputProperty(OutputKeys.INDENT, "yes");
            tF.setOutputProperty(OutputKeys.METHOD, "xml");
            tF.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            doc.normalizeDocument();
            DOMSource domS = new DOMSource(doc);
            StreamResult strR = new StreamResult(new StringWriter());
            tF.transform(domS, strR);
            resultXml = strR.getWriter().toString();
            LOGGER.debug ("XML STRING FROM MAKEXML : " + resultXml);

        } catch (ParserConfigurationException | TransformerException pe) {
            pe.printStackTrace();
        }

        LOGGER.info ("Xml creation complete for watchlist request");
        LOGGER.debug ("The Xml back to client : " + resultXml);
        return resultXml;
    }

}