package com.rivium.rms.services;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.rivium.rms.types.TrkdField;
import com.rivium.rms.types.TrkdQuote;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.w3c.dom.*;

public class QuoteService {

    private static final String W3ADDRESS = "http://www.w3.org/2005/08/addressing";
    private static final String ENDP = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Quotes_1/RetrieveItem_3";
    private static final String AUTHORIZ = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1";
    private static final String METHOD = "/api/Quotes/Quotes.svc";
    private static final String ENVELOPE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String QUOTEL = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Quotes_1";

    private String applicationId;
    private String token;
    private String quoteRequestString;
    private String ricStatus;
    private String[] wantedFieldList;
    private String privNet;
    private String connectAddress;
    private boolean allFields;

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.QuoteService");

    public QuoteService() {

    }

    public void setConnectAddress (String connectHere) { this.connectAddress = connectHere; }

    public String getConnectAddress () { return this.connectAddress; }

    public void setQuoteRequestString (String quoteList) {
        quoteRequestString = quoteList;
    }

    public String getPrivNet() {
        return privNet;
    }

    public void setPrivNet(String privNet) {
        this.privNet = privNet;
    }

    public void setAppId (String appId) {
        applicationId = appId;
    }

    public void setToken (String tok) {
        token = tok;
    }

    public String getRicStatus() {
        return this.ricStatus;
    }

    public String[] getWantedFieldList() {
        return wantedFieldList;
    }

    public void setWantedFieldList(String[] wantedFieldList) {
        this.wantedFieldList = wantedFieldList;
    }

    public boolean isAllFields() {
        return allFields;
    }

    public void setAllFields(boolean allFields) {
        this.allFields = allFields;
    }

    public TrkdQuote getQuote () {

        TrkdQuote retQuote;

        InputStream quoteResponse;

        StringBuilder fieldList = new StringBuilder();
        for (String e : wantedFieldList) {
            fieldList.append(e);
            fieldList.append(":");
        }

        String XmlData = makeRequest(applicationId, token, quoteRequestString, fieldList.toString());

        LOGGER.info ("Preparing to make request for " + quoteRequestString);
        LOGGER.debug("Request sent upstream : " + XmlData);

        try {
            quoteResponse = sendRequest(XmlData);
            LOGGER.info ("Request made. Parsing response...");
            retQuote = ParseQuoteResponse(quoteResponse);

            //n  String ret = getResponseStringBuffer(quoteResponse).toString();

            //LOGGER.debug ("RESPONSE STRING BUFFER : " + ret);

        } catch (IOException e) {
            LOGGER.debug("Quote Request Exception : " + e.getMessage());
            retQuote = null;
        }

        return retQuote;
    }

    private InputStream sendRequest(String body) throws IOException {

        String connectToHere = this.connectAddress + METHOD;

        LOGGER.debug ("Connecting to " + connectToHere);

        URL url = new URL(connectToHere);

        //String loseHttp = urlConnect.substring(6);
        String loseHttp = connectToHere.substring(6);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        OutputStreamWriter os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));

        os.write(body);
        os.flush();

        return new BufferedInputStream(connection.getInputStream());
    }

    private TrkdQuote ParseQuoteResponse(InputStream inputS) {

        ArrayList<TrkdField> fields = new ArrayList<>();

        TrkdQuote parseQuote = new TrkdQuote();

        String okRic;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputS);

            inputS.close();

            okRic = document.getElementsByTagName("omm:StatusMsg").item(0).getTextContent();

            ricStatus = okRic;

            if (!okRic.equals("OK")) {
                LOGGER.debug ("RIC ERROR -> " + quoteRequestString + " : " + ricStatus);
                parseQuote.setRicName(quoteRequestString);
                parseQuote.setRicStatus(ricStatus);

            } else {
                parseQuote.setRicStatus(ricStatus);
                NodeList nNodeL = document.getElementsByTagName("omm:RequestKey");
                Node nNode = nNodeL.item(0);
                Element elm = (Element) nNode;

                if (elm.hasAttribute("Name")) {
                    String ricName = elm.getAttribute("Name");
                    parseQuote.setRicName(ricName);
                    LOGGER.debug("RIC NAME : " + ricName);
                }

                nNodeL = document.getElementsByTagName("omm:TimelinessInfo");

                nNode = nNodeL.item(0);
                elm = (Element) nNode;

                if (elm.hasAttribute("Timeliness")) {
                    String timeL = elm.getAttribute("Timeliness");
                    parseQuote.setQoS(timeL);
                    LOGGER.debug("TIME : " + timeL);
                }

                NodeList nodeList = document.getElementsByTagName("omm:Fields");

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {

                        if (node.hasChildNodes()) {
                            NodeList chNodes = node.getChildNodes();
                            LOGGER.debug ("Fields in response : ");

                            for (int x = 0; x < chNodes.getLength(); x++) {
                                TrkdField someField = new TrkdField();
                                Node nods = chNodes.item(x);
                                String fName = nods.getAttributes().getNamedItem("Name").getNodeValue();
                                String fValue = nods.getTextContent();
                                String fDataType = nods.getAttributes().getNamedItem("DataType").getNodeValue();

                                LOGGER.debug ("Field Name : " + fName + " [ ] Value : " + fValue );

                                if (wantedFieldList.length <= 0) {

                                    someField.setFieldName(fName);
                                    someField.setFieldType(fDataType);
                                    someField.setFieldValue(fValue);

                                    fields.add(someField);
                                } else {
                                    if (Arrays.asList(wantedFieldList).contains(fName)) {
                                        someField.setFieldName(fName);
                                        someField.setFieldType(fDataType);
                                        someField.setFieldValue(fValue);
                                        fields.add(someField);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            TrkdField symB = new TrkdField();
            symB.setFieldName("SYMBOL");
            symB.setFieldValue(quoteRequestString);
            fields.add(symB);
            parseQuote.setFieldList(fields);

        } catch (IOException e) {
            LOGGER.debug("IO Exception : " + e.getMessage());

        } catch (Exception e) {
            LOGGER.debug("Exception : " + e.getMessage());
        }

        return parseQuote;
    }

    private String makeRequest(String theAppId, String theToken, String ricName, String fieldList) {

        String outX = "";

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            Document doc = docB.newDocument();
            Element envelope = doc.createElement("Envelope");
            Attr xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENVELOPE);
            envelope.setAttributeNode(xmlns);
            doc.appendChild(envelope);

            Element header = doc.createElement("Header");
            envelope.appendChild(header);

            Element to = doc.createElement("To");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            to.setAttributeNode(xmlns);
            to.setTextContent(this.connectAddress + METHOD);
            header.appendChild(to);

            Element messageId = doc.createElement("MessageID");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            messageId.setAttributeNode(xmlns);
            messageId.setTextContent("###BEAST###UNIQ###");
            header.appendChild(messageId);

            Element action = doc.createElement("Action");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            action.setAttributeNode(xmlns);
            action.setTextContent(ENDP);
            header.appendChild(action);

            Element auth = doc.createElement("Authorization");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(AUTHORIZ);
            auth.setAttributeNode(xmlns);
            header.appendChild(auth);

            Element appId = doc.createElement("ApplicationID");
            appId.setTextContent(theAppId);
            auth.appendChild(appId);

            Element token = doc.createElement("Token");
            token.setTextContent(theToken);
            auth.appendChild(token);

            Element body = doc.createElement("Body");
            envelope.appendChild(body);

            Element getReq = doc.createElement("RetrieveItem_Request_3");

            Attr trim = doc.createAttribute("TrimResponse");
            trim.setValue("false");
            getReq.setAttributeNode(trim);

            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(QUOTEL);
            getReq.setAttributeNode(xmlns);

            Element itemReq = doc.createElement("ItemRequest");
            Attr scope = doc.createAttribute("Scope");
            scope.setValue("List");
            itemReq.setAttributeNode(scope);

            Element fields = doc.createElement("Fields");
            getReq.appendChild(fields);
            fields.setTextContent(fieldList);
            itemReq.appendChild(fields);
            getReq.appendChild(itemReq);

            Element reqKey = doc.createElement("RequestKey");
            Attr name = doc.createAttribute("Name");
            name.setValue(ricName);
            reqKey.setAttributeNode(name);

            Attr nametype = doc.createAttribute("NameType");
            nametype.setValue("RIC");
            reqKey.setAttributeNode(nametype);

            itemReq.appendChild(reqKey);

            body.appendChild(getReq);

            try {
                Source xmlInput = new DOMSource(doc);
                StringWriter stringWriter = new StringWriter();
                StreamResult xmlOutput = new StreamResult(stringWriter);
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                transformerFactory.setAttribute("indent-number", 4);
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(xmlInput, xmlOutput);
               //System.out.println(xmlOutput.getWriter().toString());
                outX = xmlOutput.getWriter().toString();

            } catch (TransformerException e) {
                throw new RuntimeException(e);
            }



        } catch (ParserConfigurationException pe) {
            pe.printStackTrace();
            LOGGER.debug ("Error forming XML when making request.");
        }

        return outX;
    }

    private static StringBuffer getResponseStringBuffer(InputStream is) throws IOException
    {
        StringBuffer retval = new StringBuffer();

        int bytesRead;
        byte[] buffer = new byte[500*1024];

        while ((bytesRead = is.read(buffer)) != -1) {
            retval.append(new String(buffer, 0, bytesRead));
        }

        return retval;
    }

}
