package com.rivium.rms.services;

/*
 * Created by alain on 1/28/16.
 */

import com.rivium.rms.types.TrkdField;
import com.rivium.rms.types.TrkdQuote;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.TreeMap;
import java.util.List;


public class WatchListService {

    private static final String W3ADDRESS = "http://www.w3.org/2005/08/addressing";
    private static final String ENDP = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Quotes_1/RetrieveItem_3";
    private static final String AUTHORIZ = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1";
    private static final String METHOD = "/api/Quotes/Quotes.svc";
    private static final String ENVELOPE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String QUOTEL = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Quotes_1";

    private String applicationId;
    private String token;
    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.WatchListService");

    private List<String> quoteList;
    private List<String> requestedFieldList;

    private String listStatus;

    private String connectAddress;

    private TreeMap<String, String> theProps = new TreeMap<>();

    public WatchListService() {

    }

    public void setConnectAddress (String connectHere) { this.connectAddress = connectHere; }

    public String getConnectAddress () { return this.connectAddress; }

    public List<String> getQuoteList () {
        return quoteList;
    }

    public void setQuoteList(List<String> setList) {
        this.quoteList = setList;
    }

    public List<String> getRequestedFieldList() {
        return requestedFieldList;
    }

    public void setRequestedFieldList(List<String> requestedFieldList) {
        this.requestedFieldList = requestedFieldList;
    }

    public String getListStatus() {
        return listStatus;
    }

    public void setListStatus(String statusList) {
        this.listStatus = statusList;
    }

    public void setAppId (String appId) {
        applicationId = appId;
    }

    public String getAppId () {
        return applicationId;
    }

    public void setToken (String tok) {
        token = tok;
    }

    public String getToken () {
        return token;
    }

    public TreeMap<String, String> getTheProps() {
        return theProps;
    }

    public void setTheProps(TreeMap<String, String> theProps) {
        this.theProps = theProps;
    }

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
    }

    public ArrayList<TrkdQuote> getList () {

        ArrayList<TrkdQuote> retQuoteVals = new ArrayList<> ();

        InputStream listResponse;

        LOGGER.debug ("WATCHLIST SERVICE CALLED");

        String XmlData = makeRequest(applicationId, token);

        LOGGER.debug  ("XMLDATA FOR WATCHLIST SEND TO TR : " + XmlData);

        try {
            listResponse = sendRequest(XmlData);
            retQuoteVals = returnListOfQuotes(listResponse);
        } catch (IOException e) {
            LOGGER.debug("Watchlist Request Exception : " + e.getMessage());
        }

        return retQuoteVals;
    }

    private InputStream sendRequest(String body) throws IOException {

        URL url = new URL(connectAddress + METHOD);

        String loseHttp = connectAddress.substring(6);

        LOGGER.debug ("Connecting to " + connectAddress);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        OutputStreamWriter os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));

        os.write(body);
        os.flush();

        LOGGER.info ("WatchList was requested from TRKD.");

        //StringBuffer RetStr = getResponseStringBuffer(connection.getInputStream());

        //LOGGER.debug(RetStr);

        return new BufferedInputStream(connection.getInputStream());

    }

    private static StringBuffer getResponseStringBuffer(InputStream is) throws IOException
    {
        StringBuffer retval = new StringBuffer();

        int bytesRead;
        byte[] buffer = new byte[500*1024];

        while ((bytesRead = is.read(buffer)) != -1) {
            retval.append(new String(buffer, 0, bytesRead));
        }

        return retval;
    }

    private ArrayList<TrkdQuote> returnListOfQuotes (InputStream inputS) {

        ArrayList<TrkdQuote> quotes = new ArrayList<>();

        Document document = null;

        int startItem;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(inputS);

            inputS.close();

            LOGGER.debug("Analyzing TR data sent in response to WatchList request");

        } catch (IOException e) {
            LOGGER.debug("IO Exception : " + e.getMessage());

        } catch (Exception be) {

            LOGGER.debug("IO Exception : " + be.getMessage());
        }


        NodeList nNodeL = document != null ? document.getElementsByTagName("omm:ChildItem") : null;

        //List of child items holding fields with values

        if (nNodeL != null) {
            for (int i = 5; i < nNodeL.getLength(); i++) {
                TrkdQuote quote = new TrkdQuote();
                quote.setRicType("Quote");
                ArrayList<TrkdField> quotefields = new ArrayList<>();

                Node Childitem = nNodeL.item(i);
                Element curEl = (Element) Childitem;
                quote.setRicName(curEl.getAttribute("Name"));
                LOGGER.debug("STATUS : " + curEl.getElementsByTagName("omm:StatusMsg").item(0).getTextContent());
                quote.setRicStatus(curEl.getElementsByTagName("omm:StatusMsg").item(0).getTextContent());
                NodeList fields = curEl.getElementsByTagName("omm:Field");
                for (int c = 0; c < fields.getLength(); c++){
                    Node field = fields.item(c);
                    Element fieldEl = (Element) field;
                    TrkdField xField = new TrkdField();
                    xField.setFieldName(fieldEl.getAttribute("Name"));
                    LOGGER.debug("FIELD NAME : " + fieldEl.getAttribute("Name"));
                    NodeList values = field.getChildNodes();

                    if (values != null) {
                        for (int b = 0; b < values.getLength(); b++) {
                            Node fieldvalue = values.item(b);
                            Element val = (Element) fieldvalue;
                            xField.setFieldValue(val.getTextContent());
                        }
                    }
                    quotefields.add(xField);
                }
                quote.setFieldList(quotefields);
                quotes.add(quote);
                LOGGER.debug(quote.getRicName());
            }

//                for (TrkdQuote tQ : quotes) {
//                LOGGER.debug("WATCHLIST ITEM : " + tQ.getRicName());
//            }

        }

        if (nNodeL.getLength() == 0 ) {

            NodeList itemNodesList = document != null ? document.getElementsByTagName("omm:Item") : null;

            LOGGER.debug("ITEM NODE NUM : " + itemNodesList.getLength());

            //INDIVIDUAL RICS
            for (int x = 0; x < itemNodesList.getLength(); x++) {

                Node Item = itemNodesList.item(x);

                LOGGER.debug("NODE : " + Item.getTextContent());

                TrkdQuote quote = new TrkdQuote();
                quote.setRicType("Quote");

                LOGGER.debug ("NEW QUOTE");

                // RIC INFORMATION
                if (Item.hasChildNodes()) {

                    ArrayList<TrkdField> quotefields = new ArrayList<>();

                    NodeList children = Item.getChildNodes();

                    for (int s = 0; s < children.getLength(); s++) {
                        String ma = children.item(s).getNodeName();
                        System.out.println("CHILDNAME :" + ma);
                        if (ma.equals("omm:RequestKey")) {
                            ;
                            Node ReqKey = children.item(s);
                            NamedNodeMap fName = ReqKey.getAttributes();
                            String manip = String.valueOf(fName.getNamedItem("Name"));
                            String pN = manip.replace("Name=\"", "");
                            pN = pN.replace("\"", "");
                            quote.setRicName(pN);
                            LOGGER.debug("RIC NAME : " + pN);
                        }
                        if (ma.equals("omm:QoS")) {
                            Node QosN = children.item(s);
                            Element El = (Element) QosN;
                            NodeList ElList = El.getElementsByTagName("omm:TimelinessInfo");
                            Element Timeliness = (Element) ElList.item(0);
                            System.out.println("TIMELINESS : " + Timeliness.getAttribute("Timeliness"));
                        }
                        if (ma.equals("omm:Status")) {
                            Node QosN = children.item(s);
                            Element El = (Element) QosN;
                            NodeList ElList = El.getElementsByTagName("omm:StatusMsg");
                            Element Status = (Element) ElList.item(0);
                            //System.out.println("STATUS MSG : " + Status.getTextContent());
                            quote.setRicStatus(Status.getTextContent());
                            LOGGER.debug("RIC STATUS : " + Status.getTextContent());
                        }
                        //All Fields
                        if (ma.equals("omm:Fields")) {
                            NodeList FieldsL = children.item(s).getChildNodes();
                            for (int o = 0; o < FieldsL.getLength(); o++) {
                                TrkdField xField = new TrkdField();
                                Node FieldN = children.item(s);
                                NodeList FieldNL = FieldN.getChildNodes();
                                Node node = FieldNL.item(o);
                                if (node.getNodeType() == Node.ELEMENT_NODE) {
                                    Element FieldNames = (Element) node;
                                    String Val = FieldNames.getAttribute("Name");
                                    System.out.println("NAME : " + Val);
                                    xField.setFieldName(Val);
                                    NodeList FieldVals = node.getChildNodes();
                                    for (int z = 0; z < FieldVals.getLength(); z++) {
                                        Node fVal = FieldVals.item(z);
                                        if (fVal.getNodeType() == Node.ELEMENT_NODE) {
                                            System.out.println("FIELD NODE VALE : " + fVal.getNodeName());
                                            System.out.println("FIELD VALUE DATA : " + fVal.getTextContent());
                                            String fValue = fVal.getTextContent();
                                            xField.setFieldValue(fValue);
                                        }
                                    }
                                    quotefields.add(xField);
                                }
                                quote.setFieldList(quotefields);
                            }
                            quotes.add(quote);

                        }


                    }
                }
            }
        }

        return quotes;

    }

    private String makeRequest(String theAppId, String theToken) {

        String outX = "";

        String ric = "";

        String connectToHere = connectAddress + METHOD;

        LOGGER.debug ("WATCHLIST CONNECT: " + connectToHere);

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            Document doc = docB.newDocument();
            Element envelope = doc.createElement("Envelope");
            Attr xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENVELOPE);
            envelope.setAttributeNode(xmlns);
            doc.appendChild(envelope);

            Element header = doc.createElement("Header");
            envelope.appendChild(header);

            Element to = doc.createElement("To");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            to.setAttributeNode(xmlns);
            to.setTextContent(this.connectAddress + METHOD);
            header.appendChild(to);

            Element messageId = doc.createElement("MessageID");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            messageId.setAttributeNode(xmlns);
            messageId.setTextContent("###BEAST###UNIQ###");
            header.appendChild(messageId);

            Element action = doc.createElement("Action");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            action.setAttributeNode(xmlns);
            action.setTextContent(ENDP);
            header.appendChild(action);

            Element auth = doc.createElement("Authorization");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(AUTHORIZ);
            auth.setAttributeNode(xmlns);
            header.appendChild(auth);

            Element appId = doc.createElement("ApplicationID");
            appId.setTextContent(theAppId);
            auth.appendChild(appId);

            Element token = doc.createElement("Token");
            token.setTextContent(theToken);
            auth.appendChild(token);

            Element body = doc.createElement("Body");
            envelope.appendChild(body);

            Element getReq = doc.createElement("RetrieveItem_Request_3");

            Attr trim = doc.createAttribute("TrimResponse");
            trim.setValue("false");
            getReq.setAttributeNode(trim);

            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(QUOTEL);
            getReq.setAttributeNode(xmlns);

            for (int i = 0; i < quoteList.size(); i++) {

                Element itemReq = doc.createElement("ItemRequest");
                Attr scope = doc.createAttribute("Scope");
                scope.setValue("List");
                itemReq.setAttributeNode(scope);

                Element fields = doc.createElement("Fields");
                getReq.appendChild(fields);
                String fieldList = String.join(":", requestedFieldList);
                fields.setTextContent(fieldList);
                itemReq.appendChild(fields);
                getReq.appendChild(itemReq);

                Element reqKey = doc.createElement("RequestKey");
                Attr name = doc.createAttribute("Name");
                //URL Encode 0# 0%23
                name.setValue(quoteList.get(i));
                reqKey.setAttributeNode(name);

                Attr nametype = doc.createAttribute("NameType");
                nametype.setValue("RIC");
                reqKey.setAttributeNode(nametype);

                itemReq.appendChild(reqKey);

                body.appendChild(getReq);

                try {
                    Source xmlInput = new DOMSource(doc);
                    StringWriter stringWriter = new StringWriter();
                    StreamResult xmlOutput = new StreamResult(stringWriter);
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    transformerFactory.setAttribute("indent-number", 4);
                    Transformer transformer = transformerFactory.newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    transformer.transform(xmlInput, xmlOutput);
                    //System.out.println(xmlOutput.getWriter().toString());
                    outX = xmlOutput.getWriter().toString();

                } catch (TransformerException e) {
                    throw new RuntimeException(e);
                }
            }

        } catch (ParserConfigurationException pe) {
            pe.printStackTrace();
        }

        LOGGER.info ("XML :", outX);

        return outX;
    }
}
