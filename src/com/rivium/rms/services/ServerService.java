package com.rivium.rms.services;

import java.util.List;
import java.net.*;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.builder.fluent.Configurations;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class ServerService {

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.ServerService");
    static String[] urlsToUse;

    private static String serverName;

    public ServerService() {

    }
    public void setServerList(String[] urlsFromConfig) { this.urlsToUse = urlsFromConfig; }

    public String getServerName(){ this.serverName  = ServerisOnline(); return this.serverName; }

    public static String ServerisOnline() {

        boolean b = true;
        int port = 443;
        String ServerUrl = "";

        for (String testThisOne : urlsToUse) {
            try {
                LOGGER.debug ("TRYING TO CONNECT TO : " + testThisOne);
                Socket s = new Socket(testThisOne, port);
                s.close();
                ServerUrl = "https://" + testThisOne;
                break;
            } catch (Exception e) {
                b = false;
                LOGGER.debug("ERROR CONNECTING TO SERVER : " + testThisOne);
            }

        }
        LOGGER.debug("SERVER PICKED : " + ServerUrl);
        return ServerUrl;
    }
}
