package com.rivium.rms.services;/*
 * Created by alain on 2/20/16.
 */

import com.rivium.rms.types.TrkdNewsItem;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;


public class StoryService {

    private static final String W3ADDRESS = "http://www.w3.org/2005/08/addressing";
    private static final String ENDP = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/News_1/RetrieveStoryML_1";
    private static final String AUTHORIZ = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1";
    private static final String METHOD = "/api/News/News.svc";
    private static final String ENVELOPE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String SCHEMA = "http://www.w3.org/2001/XMLSchema";
    private static final String SCHEMAI = "http://www.w3.org/2001/XMLSchema-instance";
    private static final String STORY = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/News_1";

    private String stAppId;
    private String stToken;
    private String stQueryString;
    private String stRequestMode;
    private String stUrlConnect;
    private String connectAddress;
    
    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.StoryService");

    public String getStAppId() {
        return stAppId;
    }

    public void setStAppId(String stAppId) {
        this.stAppId = stAppId;
    }

    public void setConnectAddress (String connectHere) { this.connectAddress = connectHere; }

    public String getConnectAddress () { return this.connectAddress; }


    public String getStToken() {
        return stToken;
    }

    public void setStToken(String stToken) {
        this.stToken = stToken;
    }

    public String getStQueryString() {
        return stQueryString;
    }

    public void setStQueryString(String stQueryString) {
        this.stQueryString = stQueryString;
    }

    public String getStRequestMode() {
        return stRequestMode;
    }

    public void setStRequestMode(String stRequestMode) {
        this.stRequestMode = stRequestMode;
    }

    public String getStUrlConnect() {
        return stUrlConnect;
    }

    public void setStUrlConnect(String stUrlConnect) {
        this.stUrlConnect = stUrlConnect;
    }

    public ArrayList<TrkdNewsItem> GetTheStory() {

        InputStream TheStory;

        ArrayList<TrkdNewsItem> returnedStories = new ArrayList<>();

        LOGGER.debug ("Preparing for Story Retrieval");

        try {



            String xmldata;

            xmldata = makeRequest(stAppId, stToken, stQueryString);

            LOGGER.debug("[Story] Xml POST Data for Story Retrieval : " + xmldata);

            TheStory = sendRequest(xmldata);

            //ReturnXML = getResponseStringBuffer(TheStory).toString();

            returnedStories = parseTheStory(TheStory);

            TheStory.close();

            //String Return = getResponseStringBuffer(GottenHeadLines).toString();
            //System.out.println("Return String : " + Return);
            //LOGGER.debug ("Return code :" + Return);
            LOGGER.debug("Request succeeded. Passing on received story.");

        } catch (Exception e) {
            LOGGER.debug("Story Exception Error : " + e.getMessage());
        }

        return returnedStories;
    }

    void setQueryString (String QueryStr) {
        stQueryString = QueryStr;
    }

    private InputStream sendRequest(String body) throws IOException {

        BufferedInputStream ReturnData;
        OutputStreamWriter os;
        int ErrorCode;
        String ErrorMes;
        String connectToHere;

        connectToHere = connectAddress + METHOD;

        LOGGER.debug ("Connecting to " + connectToHere);

        URL url = new URL(connectToHere);

        String loseHttp = connectToHere.substring(6);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        try {
            os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));
            os.write(body);
            os.flush();
            ReturnData = new BufferedInputStream(connection.getInputStream());
        } catch (IOException ex){
            ErrorCode = connection.getResponseCode();
            ErrorMes = connection.getResponseMessage();
            LOGGER.debug("Server returned ErrorCode : " + ErrorCode + " -> " + ErrorMes);
            ReturnData = new BufferedInputStream(connection.getErrorStream());
        }

        return ReturnData;
    }

    private static StringBuffer getResponseStringBuffer(InputStream is) throws IOException {
        StringBuffer retval = new StringBuffer();

        int bytesRead;
        byte[] buffer = new byte[500*1024];

        while ((bytesRead = is.read(buffer)) != -1) {
            retval.append(new String(buffer, 0, bytesRead));
        }

        return retval;
    }

    private String makeRequest(String AppId, String daToken, String storyId) {

        String outX = "";

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            Document doc = docB.newDocument();
            Element envelope = doc.createElement("Envelope");
            Attr xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENVELOPE);
            envelope.setAttributeNode(xmlns);
            doc.appendChild(envelope);

            Element header = doc.createElement("Header");
            envelope.appendChild(header);

            Element to = doc.createElement("To");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            to.setAttributeNode(xmlns);
            to.setTextContent(connectAddress + METHOD);
            header.appendChild(to);

            Element messageId = doc.createElement("MessageID");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            messageId.setAttributeNode(xmlns);
            messageId.setTextContent("###BEAST###UNIQ###");
            header.appendChild(messageId);

            Element action = doc.createElement("Action");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            action.setAttributeNode(xmlns);
            action.setTextContent(ENDP);
            header.appendChild(action);

            Element auth = doc.createElement("Authorization");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(AUTHORIZ);
            auth.setAttributeNode(xmlns);
            header.appendChild(auth);

            Element appId = doc.createElement("ApplicationID");
            appId.setTextContent(AppId);
            auth.appendChild(appId);

            Element token = doc.createElement("Token");
            token.setTextContent(daToken);
            auth.appendChild(token);

            Element body = doc.createElement("Body");
            envelope.appendChild(body);

            Element storyReq = doc.createElement("RetrieveStoryML_Request_1");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(STORY);
            storyReq.setAttributeNode(xmlns);
            body.appendChild(storyReq);

            Attr xsd = doc.createAttribute("xmlns:xsd");
            xsd.setValue(SCHEMA);
            storyReq.setAttributeNode(xsd);

            Attr xsi = doc.createAttribute("xmlns:xsi");
            xsi.setValue(SCHEMAI);
            storyReq.setAttributeNode(xsi);

            Element storyML = doc.createElement("StoryMLRequest");
            storyReq.appendChild(storyML);

            Element timeO = doc.createElement("TimeOut");
            timeO.setTextContent("600");
            storyML.appendChild(timeO);

            Element id = doc.createElement("StoryId");
            id.setTextContent(storyId);
            storyML.appendChild(id);

            try {
                Source xmlInput = new DOMSource(doc);
                StringWriter stringWriter = new StringWriter();
                StreamResult xmlOutput = new StreamResult(stringWriter);
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                transformerFactory.setAttribute("indent-number", 4);
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(xmlInput, xmlOutput);
                System.out.println(xmlOutput.getWriter().toString());
                outX = xmlOutput.getWriter().toString();

            } catch (TransformerException e) {
                throw new RuntimeException(e);
            }

        } catch (ParserConfigurationException pe) {
            pe.printStackTrace();
        }

        return outX;
    }

    private ArrayList<TrkdNewsItem> parseTheStory (InputStream inputS) {

        ArrayList<TrkdNewsItem> StoryLines = new ArrayList<>();
        Document document = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(inputS);
            inputS.close();
            document.getDocumentElement().normalize();
        } catch (IOException io){
            LOGGER.debug("IO Exception : " + io.getMessage());
        } catch (org.xml.sax.SAXException | ParserConfigurationException e) {
            LOGGER.debug("SAX Exception : " + e.getMessage());
        }

        String okStoryquery = document != null ? document.getElementsByTagName("ns0:StatusMsg").item(0).getTextContent() : null;

        System.err.println("Story Status : " + okStoryquery);

        if (!(okStoryquery != null && okStoryquery.matches("OK"))){
            LOGGER.debug ("Story ERROR -> " + stQueryString + " : " + okStoryquery);
        } else {
            TrkdNewsItem hLine = new TrkdNewsItem();
            NodeList storyS = document.getElementsByTagName("HL");
            if (storyS.getLength() == 0) {
                String bummer = "Information currently unavailable";
                LOGGER.debug("NO STORY FOUND");
                hLine.setID(stQueryString);
                hLine.setCT(bummer);
                hLine.setRT(bummer);
                hLine.setItemType("News");
                hLine.setST("USABLE");
                hLine.setUR("4");
                hLine.setLN("en");
                hLine.setHT(bummer);
                hLine.setTE(bummer);
                StoryLines.add(hLine);
            } else {
                for (int q = 0; q < storyS.getLength(); q++) {
                    Node storyHead = storyS.item(q);
                    LOGGER.debug("NODE NAME : " + storyHead.getNodeName());
                    if (storyHead.hasChildNodes()) {
                        NodeList storyProps = storyHead.getChildNodes();
                        for (int w = 0; w < storyProps.getLength(); w++) {
                            Node storyItem = storyProps.item(w);
                            LOGGER.debug("HEADLINE NODE NAME : " + storyItem.getNodeName());
                            String storyItemName = storyItem.getNodeName();

                            if (storyItemName.matches("ID")) {
                                hLine.setID(storyItem.getTextContent());
                            } else if (storyItemName.matches("RE")) {
                                hLine.setRE(storyItem.getTextContent());
                            } else if (storyItemName.matches("ST")) {
                                hLine.setST(storyItem.getTextContent());
                            } else if (storyItemName.matches("CT")) {
                                hLine.setCT(storyItem.getTextContent());
                            } else if (storyItemName.matches("RT")) {
                                hLine.setRT(storyItem.getTextContent());
                            } else if (storyItemName.matches("PR")) {
                                hLine.setPR(storyItem.getTextContent());
                            } else if (storyItemName.matches("AT")) {
                                hLine.setAT(storyItem.getTextContent());
                            } else if (storyItemName.matches("UR")) {
                                hLine.setUR(storyItem.getTextContent());
                            } else if (storyItemName.matches("LN")) {
                                hLine.setLN(storyItem.getTextContent());
                            } else if (storyItemName.matches("HT")) {
                                hLine.setHT(storyItem.getTextContent());
                            } else if (storyItemName.matches("PE")) {
                                hLine.setPE(storyItem.getTextContent());
                            } else if (storyItemName.matches("CO")) {
                                hLine.setCO(storyItem.getTextContent());
                            } else if (storyItemName.matches("TN")) {
                                hLine.setTN(storyItem.getTextContent());
                            } else if (storyItemName.matches("TE")) {
                                hLine.setTE(storyItem.getTextContent());
                            }
                        }
                    }

                    StoryLines.add(hLine);
                }
            }
        }

        return StoryLines;
    }
}

