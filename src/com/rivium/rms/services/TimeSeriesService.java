package com.rivium.rms.services;

/*
 * Created by alain on 1/28/16.
 */
import com.rivium.rms.types.TrkdField;
import com.rivium.rms.types.TrkdQuote;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


public class TimeSeriesService {

    private static final String W3ADDRESS = "http://www.w3.org/2005/08/addressing";
    private static final String ENDP = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/TimeSeries_1/GetIntradayTimeSeries_2";
    private static final String AUTHORIZ = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1";
    private static final String METHOD = "/api/TimeSeries/TimeSeries.svc";
    private static final String ENVELOPE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String SCHEMA = "http://www.w3.org/2001/XMLSchema";
    private static final String SCHEMAI = "http://www.w3.org/2001/XMLSchema-instance";
    private static final String TIMEL = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/TimeSeries_1";

    private String applicationId;
    private String token;
    private String symBol;
    private String startTime;
    private String stopTime;
    private String interValPeriod;
    private String privNet;
    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.TimeSeriesService");
    private String urlConnect;
    private String connectAddress;

    public TimeSeriesService() {

    }


    public void setConnectAddress (String connectHere) { this.connectAddress = connectHere; }

    public String getConnectAddress () { return this.connectAddress; }

    public void setApplicationId (String appId) {
        this.applicationId = appId;
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public void setToken (String tok) {
        this.token = tok;
    }

    public String getToken() {
        return this.token;
    }

    public void setsymBol (String qRic) {
        this.symBol = qRic;
    }

    public String getsymBol() {
        return this.symBol;
    }

    public void setStartTime (String theDate) {
        this.startTime = theDate;
    }

    public String getStartTime() {
        return this.startTime;
    }

    public void setStopTime (String theDate) {
        this.stopTime = theDate;
    }

    public String getStopTime() {
        return this.stopTime;
    }

    public void setInterValPeriod (String intPeriod) {
        this.interValPeriod = intPeriod;
    }

    public String getInterValPeriod() {
        return this.interValPeriod;
    }

    public ArrayList<TrkdQuote> getTimeSeries() {

        ArrayList<TrkdQuote> retVal = new ArrayList<>();

        InputStream timeSeriesResponse;

        String xmlData;

        xmlData = makeRequest(applicationId, token, symBol, startTime, stopTime, interValPeriod);

        LOGGER.debug ("TIMESERIES REQUEST STRING : \n" + xmlData);

        try {
            timeSeriesResponse = sendRequest(xmlData);
            //StringBuffer line = getResponseStringBuffer(timeSeriesResponse);
            //System.out.println ("The response : " + line);
            retVal = ParseTimeSeriesResponse(timeSeriesResponse);
        } catch (IOException e) {
            LOGGER.debug ("TimeSeries Request Exception : " + e.getMessage());
        }

        return retVal;
    }

    private InputStream sendRequest(String body) throws IOException {


        String connectToHere = this.connectAddress + METHOD;

        LOGGER.debug ("Connecting to " + connectToHere);

        URL url = new URL(connectToHere);

        String loseHttp = connectToHere.substring(6);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        OutputStreamWriter os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));

        os.write(body);
        os.flush();

        return new BufferedInputStream(connection.getInputStream());
    }

    private static StringBuffer getResponseStringBuffer(InputStream is) throws IOException
    {
        StringBuffer retval = new StringBuffer();

        int bytesRead;
        byte[] buffer = new byte[500*1024];

        while ((bytesRead = is.read(buffer)) != -1) {
            retval.append(new String(buffer, 0, bytesRead));
        }

        return retval;
    }

    private ArrayList<TrkdQuote>  ParseTimeSeriesResponse(InputStream inputS) {

        ArrayList<TrkdQuote> chartRows = new ArrayList<>();

        Document doc = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(inputS);
            inputS.close();
        } catch (Exception e) {
            LOGGER.debug ("XML Error: " + e.getMessage());
        }

        NodeList rows = doc != null ? doc.getElementsByTagName("Row") : null;

        for (int a = 0; a < (rows != null ? rows.getLength() : 0); a++) {
            Node row = rows.item(a);
            TrkdQuote seriesRow = new TrkdQuote();
            seriesRow.setRicName(row.getNodeName());
            //seriesRow.setRicName(row.getTextContent());
            ArrayList<TrkdField> seriesFields = new ArrayList<>();
            LOGGER.debug ("ROW NODE +: " + row.getNodeName());
            if (row.hasChildNodes()){
                NodeList rowFields = row.getChildNodes();
                for (int b = 0; b < rowFields.getLength(); b++) {
                    Node rowField = rowFields.item(b);
                    TrkdField rowF = new TrkdField();
                    rowF.setFieldName(rowField.getNodeName());
                    rowF.setFieldValue(rowField.getTextContent());
                    seriesFields.add(rowF);
                    LOGGER.debug("ROW FIELDS : " + rowField.getNodeName() + " [ ] Value : " + rowField.getTextContent());
                }

                seriesRow.setFieldList(seriesFields);
            }

            chartRows.add(seriesRow);
        }

        return chartRows;
    }

    private String makeRequest(String theAppId, String theToken, String theRic, String theStartTime, String theEndTime, String theInterval) {

        String outX = "";

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            Document doc = docB.newDocument();
            Element envelope = doc.createElement("Envelope");
            Attr xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENVELOPE);
            envelope.setAttributeNode(xmlns);
            doc.appendChild(envelope);

            Element header = doc.createElement("Header");
            envelope.appendChild(header);

            Element to = doc.createElement("To");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            to.setAttributeNode(xmlns);
            to.setTextContent(connectAddress + METHOD);
            header.appendChild(to);

            Element messageId = doc.createElement("MessageID");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            messageId.setAttributeNode(xmlns);
            messageId.setTextContent("###BEAST###UNIQ###");
            header.appendChild(messageId);

            Element action = doc.createElement("Action");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            action.setAttributeNode(xmlns);
            action.setTextContent(ENDP);
            header.appendChild(action);

            Element auth = doc.createElement("Authorization");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(AUTHORIZ);
            auth.setAttributeNode(xmlns);
            header.appendChild(auth);

            Element appId = doc.createElement("ApplicationID");
            appId.setTextContent(theAppId);
            auth.appendChild(appId);

            Element token = doc.createElement("Token");
            token.setTextContent(theToken);
            auth.appendChild(token);

            Element body = doc.createElement("Body");
            envelope.appendChild(body);

            Element intra = doc.createElement("GetInterdayTimeSeries_Request_3");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(TIMEL);
            intra.setAttributeNode(xmlns);

            Attr xsd = doc.createAttribute("xmlns:xsd");
            xsd.setValue(SCHEMA);
            intra.setAttributeNode(xsd);

            Attr xsi = doc.createAttribute("xmlns:xsi");
            xsi.setValue(SCHEMAI);
            intra.setAttributeNode(xsi);

            body.appendChild(intra);

            Element symB = doc.createElement("Symbol");
            symB.setTextContent(theRic);
            intra.appendChild(symB);

            Element startT = doc.createElement("StartTime");
            startT.setTextContent(theStartTime);
            intra.appendChild(startT);

            Element stopT = doc.createElement("EndTime");
            stopT.setTextContent(theEndTime);
            intra.appendChild(stopT);

            Element interV = doc.createElement("Interval");
            interV.setTextContent(theInterval);
            intra.appendChild(interV);

            try {
                Source xmlInput = new DOMSource(doc);
                StringWriter stringWriter = new StringWriter();
                StreamResult xmlOutput = new StreamResult(stringWriter);
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                transformerFactory.setAttribute("indent-number", 4);
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.transform(xmlInput, xmlOutput);
                //System.out.println(xmlOutput.getWriter().toString());
                outX = xmlOutput.getWriter().toString();

            } catch (TransformerException e) {
                throw new RuntimeException(e);
            }

        } catch (ParserConfigurationException pe) {
            pe.printStackTrace();
        }

        return outX;
    }
}