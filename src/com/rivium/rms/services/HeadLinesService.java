package com.rivium.rms.services;/*
 * Created by alain on 2/20/16.
 */
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.rivium.rms.types.TrkdNewsItem;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


public class HeadLinesService {

    private static final String W3ADDRESS = "http://www.w3.org/2005/08/addressing";
    private static final String ENDP = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/News_1";
    private static final String AUTHORIZ = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1";
    private static final String METHOD = "/api/News/News.svc";
    private static final String ENVELOPE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String NEWSL = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/News_1/RetrieveHeadlineML_1";
    private static final String FILTER = "http://schemas.reuters.com/ns/2006/04/14/rmds/webservices/news/filter";

    private String hlAppId;
    private String hlToken;
    private String hlQueryString;
    private String connectAddress;

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.HeadLinesService");

    public void setHlAppId(String hlAppId) {
        this.hlAppId = hlAppId;
    }

    public void setHlToken(String hlToken) {
        this.hlToken = hlToken;
    }

    public void setConnectAddress (String connectHere) { this.connectAddress = connectHere; }

    public String getConnectAddress () { return this.connectAddress; }

    public void setHlQueryString(String hlQueryString) {
        this.hlQueryString = hlQueryString;
    }

    public ArrayList<TrkdNewsItem> GetTheHeadLines() {

        InputStream GottenHeadLines;
        ArrayList<TrkdNewsItem> retHeads = new ArrayList<>();

        LOGGER.debug("Searching for filter " + hlQueryString);

       // try {
            LOGGER.debug("HL REQ : Requesting headlines");
            String xmldata;

            xmldata = makeRequest(hlAppId, hlToken, hlQueryString);

            LOGGER.debug ("The NewsQ XML : " + xmldata);
        System.err.println ("The NewsQ XML : " + xmldata);

        try {
            GottenHeadLines = sendRequest(xmldata);

            retHeads = parseTheHeadLines(GottenHeadLines);

            //String Return = getResponseStringBuffer(GottenHeadLines).toString();
            //System.out.println("Return String : " + Return);
            //LOGGER.debug ("Return code :" + Return);
            LOGGER.debug("Request succeeded. Passing on received headlines...");

        } catch (Exception e) {
            System.err.println(hlAppId);
            System.err.println(hlToken);
            System.err.println(hlQueryString);
            System.err.println("HeadLinesError: " + e.getMessage());
            e.printStackTrace();
            LOGGER.debug("Exception Error : " + e.getMessage());
            //return GottenHeadLines;
        }
        return retHeads;
    }

   // void setRequestMode(String Request) {
   //     hlRequestMode = Request;
   // }

    private InputStream sendRequest(String body) throws IOException {

        BufferedInputStream ReturnData;
        OutputStreamWriter os;
        int ErrorCode;
        String ErrorMes;
        String connectToHere = "";

        connectToHere = connectAddress + METHOD;

        //LOGGER.debug ("Connecting to " + urlConnect + "/api/2006/05/01/News_1.svc");
        LOGGER.debug ("Connecting to " + connectToHere);
        System.err.println ("Connecting to " + connectToHere);

        //URL url = new URL(urlConnect + "/api/2006/05/01/News_1.svc");
        URL url = new URL(connectToHere);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        String loseHttp = connectToHere.substring(6);

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        try {
            os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));
            os.write(body);
            os.flush();
            ReturnData = new BufferedInputStream(connection.getInputStream());
        } catch (IOException ex){
            ErrorCode = connection.getResponseCode();
            ErrorMes = connection.getResponseMessage();
            LOGGER.debug("Server returned ErrorCode : " + ErrorCode + " -> " + ErrorMes);
            ReturnData = new BufferedInputStream(connection.getErrorStream());
        }

        return ReturnData;
    }

    private static StringBuffer getResponseStringBuffer(InputStream is) throws IOException
    {
        StringBuffer retval = new StringBuffer();

        int bytesRead;
        byte[] buffer = new byte[500*1024];

        while ((bytesRead = is.read(buffer)) != -1) {
            retval.append(new String(buffer, 0, bytesRead));
        }

        return retval;
    }

    private ArrayList<TrkdNewsItem> parseTheHeadLines (InputStream inputS) {

        ArrayList<TrkdNewsItem> headLines = new ArrayList<>();
        Document document = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(inputS);
            inputS.close();
            document.getDocumentElement().normalize();
        } catch (IOException io){
            LOGGER.debug("IO Exception : " + io.getMessage());
        } catch (org.xml.sax.SAXException | ParserConfigurationException e) {
            LOGGER.debug("SAX Exception : " + e.getMessage());
        }

        String okHlquery = document != null ? document.getElementsByTagName("ns0:StatusMsg").item(0).getTextContent() : null;

        System.err.println("THE HEADLINE STATUS : " + okHlquery);

        if (!(okHlquery != null && okHlquery.matches("OK"))){
            LOGGER.debug ("Headline ERROR -> " + hlQueryString + " : " + okHlquery);
        } else {
            NodeList headS = document.getElementsByTagName("HL");
            if (headS.getLength() == 0){
                System.err.println("NOTHING FOUND");
            }

            for (int q = 0; q < headS.getLength(); q++) {
                Node aHead = headS.item(q);
                System.err.println("NODE NAME : " + aHead.getNodeName());
                TrkdNewsItem hLine = new TrkdNewsItem();
                hLine.setItemType("headline");

                if (aHead.hasChildNodes()) {
                    NodeList headLineProps = aHead.getChildNodes();
                    for (int w = 0; w < headLineProps.getLength(); w++) {
                        Node headItem = headLineProps.item(w);
                        System.err.println("HEADLINE NODE NAME : " + headItem.getNodeName());
                        String headlineItemName = headItem.getNodeName();

                        if (headlineItemName.matches("ID")) {
                            hLine.setID(headItem.getTextContent());
                        } else if (headlineItemName.matches("RE")) {
                            hLine.setRE(headItem.getTextContent());
                        } else if (headlineItemName.matches("ST")) {
                            hLine.setST(headItem.getTextContent());
                        } else if (headlineItemName.matches("CT")) {
                            hLine.setCT(headItem.getTextContent());
                        } else if (headlineItemName.matches("RT")) {
                            hLine.setRT(headItem.getTextContent());
                        } else if (headlineItemName.matches("PR")) {
                            hLine.setPR(headItem.getTextContent());
                        } else if (headlineItemName.matches("AT")) {
                            hLine.setAT(headItem.getTextContent());
                        } else if (headlineItemName.matches("UR")) {
                            hLine.setUR(headItem.getTextContent());
                        } else if (headlineItemName.matches("LN")) {
                            hLine.setLN(headItem.getTextContent());
                        } else if (headlineItemName.matches("HT")) {
                            hLine.setHT(headItem.getTextContent());
                        } else if (headlineItemName.matches("PE")) {
                            hLine.setPE(headItem.getTextContent());
                        } else if (headlineItemName.matches("CO")) {
                            hLine.setCO(headItem.getTextContent());
                        } else if (headlineItemName.matches("TN")) {
                            hLine.setTN(headItem.getTextContent());
                        }
                    }
                } else {

                    System.err.println("NO CHILD HEADS");
                }

                headLines.add(hLine);

            }
        }

        return headLines;
    }

    private String makeRequest(String theAppId, String theToken, String theHeadLineReq) {

        String outX = "";

        String connectHere = connectAddress + METHOD;

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            Document doc = docB.newDocument();
            Element envelope = doc.createElement("Envelope");
            Attr xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENVELOPE);
            envelope.setAttributeNode(xmlns);
            doc.appendChild(envelope);

            Element header = doc.createElement("Header");
            envelope.appendChild(header);

            Element to = doc.createElement("To");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            to.setAttributeNode(xmlns);
            to.setTextContent(connectHere);

            header.appendChild(to);

            Element messageId = doc.createElement("MessageID");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            messageId.setAttributeNode(xmlns);
            messageId.setTextContent("###BEAST###UNIQ###");
            header.appendChild(messageId);

            Element action = doc.createElement("Action");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(W3ADDRESS);
            action.setAttributeNode(xmlns);
            action.setTextContent(NEWSL);
            header.appendChild(action);

            Element auth = doc.createElement("Authorization");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(AUTHORIZ);
            auth.setAttributeNode(xmlns);
            header.appendChild(auth);

            Element appId = doc.createElement("ApplicationID");
            appId.setTextContent(theAppId);
            auth.appendChild(appId);

            Element token = doc.createElement("Token");
            token.setTextContent(theToken);
            auth.appendChild(token);

            Element body = doc.createElement("Body");
            envelope.appendChild(body);

            Element headReq = doc.createElement("RetrieveHeadlineML_Request_1");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENDP);
            headReq.setAttributeNode(xmlns);
            body.appendChild(headReq);

            Element hReq = doc.createElement("HeadlineMLRequest");
            headReq.appendChild(hReq);

            Element tOut = doc.createElement("TimeOut");
            tOut.setTextContent("600");
            hReq.appendChild(tOut);

            Element maxC = doc.createElement("MaxCount");
            maxC.setTextContent("30");
            hReq.appendChild(maxC);

            Element filter = doc.createElement("Filter");
            hReq.appendChild(filter);

            Element mDataCon = doc.createElement("MetaDataConstraint");
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(FILTER);
            mDataCon.setAttributeNode(xmlns);

            Attr cls = doc.createAttribute("class");
            cls.setValue("companies");
            mDataCon.setAttributeNode(cls);
            filter.appendChild(mDataCon);

            Element value = doc.createElement("Value");
            value.setTextContent(theHeadLineReq);
            mDataCon.appendChild(value);

            try {
                Source xmlInput = new DOMSource(doc);
                StringWriter stringWriter = new StringWriter();
                StreamResult xmlOutput = new StreamResult(stringWriter);
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                transformerFactory.setAttribute("indent-number", 4);
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(xmlInput, xmlOutput);
                //System.out.println(xmlOutput.getWriter().toString());
                outX = xmlOutput.getWriter().toString();

            } catch (TransformerException e) {
                throw new RuntimeException(e);
            }

        } catch (ParserConfigurationException pe) {
            pe.printStackTrace();
        }

        return outX;
    }
}

