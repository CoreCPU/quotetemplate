package com.rivium.rms.services;


import com.rivium.rms.types.TrkdChart;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class ChartService {

    private static final String W3ADDRESS = "http://www.w3.org/2005/08/addressing";
    private static final String ENDP = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Charts_1/GetChart_2";
    private static final String AUTHORIZ = "http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1";
    private static final String METHOD = "/api/Charts/Charts.svc";
    private static final String ENVELOPE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String SCHEMA = "http://www.w3.org/2001/XMLSchema";
    private static final String SCHEMAI = "http://www.w3.org/2001/XMLSchema-instance";

    private String applicationId;
    private String token;
    private String theRic;
    private String startDate = "";
    private String stopDate = "";
    private String volumeYN = "";
    private String theAnalysis = "";
    private String connectAddress = "";
    private String privNet;

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.ChartService");

    public final void setApplicationID(String appId) {
        this.applicationId = appId;
    }

    public final void setToken (String daToken) {
        this.token = daToken;
    }

    public final void setRic(String aRic) {
        this.theRic = aRic;
    }

    public void setConnectAddress (String connectHere) { this.connectAddress = connectHere; }

    public String getConnectAddress () { return this.connectAddress; }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }

    public void setVolumeYN(String volumeYN) {
        this.volumeYN = volumeYN;
    }

    public void setAnalysis(String analysis) {
        this.theAnalysis = analysis;
    }

    public String getPrivNet() {
        return privNet;
    }

    public void setPrivNet(String privNet) {
        this.privNet = privNet;
    }

    public TrkdChart getChart() {

        InputStream chartResponse;
        String chartLocation = "";
        TrkdChart returnChart = new TrkdChart();

        String xmlString = makeXml(applicationId, token, theRic, startDate, stopDate);

        LOGGER.debug("CHART REQUEST STRING : " + xmlString);

        try {
            chartResponse = sendRequest(xmlString);
            //StringBuffer line = getResponseStringBuffer(chartResponse);
            //System.out.println ("The response : " + line);
            //ParseQuoteResponse(quoteResponse);
            returnChart = parseChartUrl(chartResponse);
            LOGGER.debug ("CHART LOCATION : " + chartLocation);

        } catch (IOException e) {
            LOGGER.debug("Chart Request Exception : " + e.getMessage());
        }

        return returnChart;
    }

    private InputStream sendRequest(String body) throws IOException {

        String connectToHere = connectAddress + METHOD;

        LOGGER.debug ("Connecting to " + connectToHere);

        URL url = new URL(connectToHere);

        String loseHttp = connectToHere.substring(6);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        OutputStreamWriter os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));

        os.write(body);
        os.flush();

        return new BufferedInputStream(connection.getInputStream());
    }

    private TrkdChart parseChartUrl (InputStream chartStream) {

        TrkdChart parsedChart = new TrkdChart();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(chartStream);

            chartStream.close();

            //Element urlElement = document.ge("ChartImageResult");
            NodeList urlNodeList = document.getElementsByTagName("ChartImageResult");

            for (int q = 0; q < urlNodeList.getLength(); q++) {
                Element urlElement = (Element) urlNodeList.item(q);

                if (urlElement.hasAttributes()) {
                    String pngUrl = urlElement.getAttribute("Url");
                    String pngHttpsUrl = urlElement.getAttribute("SecureUrl");
                    String mplsPngUrl = urlElement.getAttribute("MPLSURL");
                    String mplsPngHttpsUrl = urlElement.getAttribute("SecureMPLSURL");
                    String imageTag = urlElement.getAttribute("Tag");
                    String imageServer = urlElement.getAttribute("Server");
                    System.err.println("PMGURL : " + pngUrl);
                    parsedChart.setHttpUrl(pngUrl);
                    parsedChart.setHttpsUrl(pngHttpsUrl);
                    parsedChart.setMplsHttpUrl(mplsPngUrl);
                    parsedChart.setMplsHttpsUrl(mplsPngHttpsUrl);
                    parsedChart.setImageTag(imageTag);
                    parsedChart.setServer(imageServer);
                } else {
                    System.err.println("No url element found !");
                }
            }

        } catch (IOException ie){
            LOGGER.debug ("IO Exception caught during parsing of Chart stream");
        } catch (Exception e) {
            LOGGER.debug ("XML Parse Error encountered.");
            e.printStackTrace();
        }


        return parsedChart;
    }

    private String makeXml(String theAppId, String theToken, String chartRic, String theStartDate, String theStopDate) {

        String outX = "";

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();
            Document doc = docB.newDocument();
            Element envelope = doc.createElement("Envelope");
            Attr xmlns = doc.createAttribute("xmlns");
            xmlns.setValue(ENVELOPE);
            envelope.setAttributeNode(xmlns);

            Attr xmlnsxsd = doc.createAttribute("xmlns:xsd");
            xmlnsxsd.setValue(SCHEMA);
            envelope.setAttributeNode(xmlnsxsd);

            Attr xmlnsxsi = doc.createAttribute("xmlns:xsi");
            xmlnsxsi.setValue(SCHEMAI);
            envelope.setAttributeNode(xmlnsxsi);

            doc.appendChild(envelope);

            Element header = doc.createElement("Header");
            envelope.appendChild(header);

            Element to = doc.createElement("To");
            Attr xmlns1 = doc.createAttribute("xmlns");
            xmlns1.setValue(W3ADDRESS);
            to.setAttributeNode(xmlns1);
            to.setTextContent(connectAddress + METHOD);
            header.appendChild(to);

            Element messageId = doc.createElement("MessageID");
            Attr xmlns2 = doc.createAttribute("xmlns");
            xmlns2.setValue(W3ADDRESS);
            messageId.setAttributeNode(xmlns2);
            messageId.setTextContent("###BEAST###UNIQ###");
            header.appendChild(messageId);

            Element action = doc.createElement("Action");
            Attr xmlns3 = doc.createAttribute("xmlns");
            xmlns3.setValue(W3ADDRESS);
            action.setAttributeNode(xmlns3);
            action.setTextContent(ENDP);
            header.appendChild(action);

            Element auth = doc.createElement("Authorization");
            Attr xmlns4 = doc.createAttribute("xmlns");
            xmlns4.setValue(AUTHORIZ);
            auth.setAttributeNode(xmlns4);
            header.appendChild(auth);

            Element appId = doc.createElement("ApplicationID");
            appId.setTextContent(theAppId);
            auth.appendChild(appId);

            Element token = doc.createElement("Token");
            token.setTextContent(theToken);
            auth.appendChild(token);

            Element body = doc.createElement("Body");
            envelope.appendChild(body);

            Element reQ = doc.createElementNS("http://www.reuters.com/ns/2006/05/01/webservices/rkd/Charts_1","GetChart_Request_2");
            body.appendChild(reQ);

            Element chReq = doc.createElement("chartRequest");
            reQ.appendChild(chReq);
            Attr iType = doc.createAttribute("ImageType");
            iType.setValue("PNG");
            chReq.setAttributeNode(iType);

            Attr iWidth = doc.createAttribute("Width");
            iWidth.setValue("500");
            chReq.setAttributeNode(iWidth);

            Attr iHeight = doc.createAttribute("Height");
            iHeight.setValue("400");
            chReq.setAttributeNode(iHeight);

            Attr privU = doc.createAttribute("ReturnPrivateNetworkURL");
            privU.setValue("false");
            chReq.setAttributeNode(privU);

            Element timeS = doc.createElement("TimeSeries");
            chReq.appendChild(timeS);

            Element timeSReq = doc.createElement("TimeSeriesRequest");
            timeS.appendChild(timeSReq);

            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue("http://metastock.com/imageserver/financial");
            timeSReq.setAttributeNode(xmlns);

            Element symbol = doc.createElement("Symbol");
            symbol.setTextContent(chartRic);
            timeSReq.appendChild(symbol);

            Element refR = doc.createElement("Reference");
            refR.setTextContent("d0");
            timeSReq.appendChild(refR);

            Element Analyses = doc.createElement("Analyses");
            chReq.appendChild(Analyses);

            Element analysis = doc.createElement("Analysis");
            Analyses.appendChild(analysis);
            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue("http://metastock.com/imageserver/analysis");
            analysis.setAttributeNode(xmlns);

            Element ref2 = doc.createElement("Reference");
            ref2.setTextContent("a0");
            analysis.appendChild(ref2);

            //Element ohlc = doc.createElement("OHLC");
            Element ohlc = doc.createElement(theAnalysis);
            analysis.appendChild(ohlc);

            Element instr = doc.createElement("Instrument1");
            ohlc.appendChild(instr);

            Element ref1 = doc.createElement("Reference");
            ref1.setTextContent("d0");
            instr.appendChild(ref1);

            analysis = doc.createElement("Analysis");
            Analyses.appendChild(analysis);

            xmlns = doc.createAttribute("xmlns");
            xmlns.setValue("http://metastock.com/imageserver/analysis");
            analysis.setAttributeNode(xmlns);

            Element ref3 = doc.createElement("Reference");
            ref3.setTextContent("a-1");
            analysis.appendChild(ref3);

            Element vol = doc.createElement("Vol");
            analysis.appendChild(vol);

            instr = doc.createElement("Instrument1");
            vol.appendChild(instr);

            Element ref4 = doc.createElement("Reference");
            ref4.setTextContent("d0");
            instr.appendChild(ref4);

            Element stdT = doc.createElement("StandardTemplate");
            stdT.setAttribute("xmlns", "http://metastock.com/imageserver/financial");
            chReq.appendChild(stdT);

            Element title = doc.createElement("Title");
            stdT.appendChild(title);

            Element cap = doc.createElement("Caption");
            title.appendChild(cap);

            Element visR = doc.createElement("Visible");
            visR.setTextContent("true");
            cap.appendChild(visR);

            Element cust = doc.createElement("Customized");
            cust.setTextContent("false");
            cap.appendChild(cust);

            Element range = doc.createElement("Range");
            title.appendChild(range);

            Element visA = doc.createElement("Visible");
            visA.setTextContent("true");
            range.appendChild(visA);

            Element leg = doc.createElement("Legend");
            stdT.appendChild(leg);

            Element vis1 = doc.createElement("Visible");
            vis1.setTextContent("true");
            leg.appendChild(vis1);

            Element info = doc.createElement("Information");
            info.setTextContent("Long");
            leg.appendChild(info);

            Element lay = doc.createElement("Layout");
            lay.setTextContent("MultiLine");
            leg.appendChild(lay);

            Element posA = doc.createElement("Position");
            posA.setTextContent("Overlaid");
            leg.appendChild(posA);

            instr = doc.createElement("Instrument");
            instr.setTextContent("Symbol");
            stdT.appendChild(instr);

            Element del = doc.createElement("Delimiter");
            del.setTextContent("%");
            stdT.appendChild(del);

            Element grid = doc.createElement("GridLines");
            grid.setTextContent("Both");
            stdT.appendChild(grid);

            Element yA = doc.createElement("YAxisMarkers");
            yA.setTextContent("None");
            stdT.appendChild(yA);

            Element yaT = doc.createElement("YAxisTitles");
            yaT.setTextContent("All All");
            stdT.appendChild(yaT);

            Element bR = doc.createElement("Brand");
            bR.setTextContent("None");
            stdT.appendChild(bR);

            Element interV = doc.createElement("Interval");
            stdT.appendChild(interV);

            Element commonT = doc.createElement("CommonType");
            commonT.setTextContent("Days");
            interV.appendChild(commonT);

            Attr x = doc.createAttribute("xmlns");
            x.setValue("http://metastock.com/applications/data");
            commonT.setAttributeNode(x);

            Element multI = doc.createElement("Multiplier");
            Attr y = doc.createAttribute("xmlns");
            y.setValue("http://metastock.com/applications/data");
            multI.setAttributeNode(y);
            multI.setTextContent("1");
            interV.appendChild(multI);

            Element sowN = doc.createElement("ShowNonTradedPeriods");
            sowN.setTextContent("false");
            stdT.appendChild(sowN);

            Element sowH = doc.createElement("ShowHolidays");
            sowH.setTextContent("false");
            stdT.appendChild(sowH);

            Element sowG = doc.createElement("ShowGaps");
            sowG.setTextContent("false");
            stdT.appendChild(sowG);

            Element xA = doc.createElement("XAxis");
            stdT.appendChild(xA);
            Element visQ = doc.createElement("Visible");
            visQ.setTextContent("true");
            xA.appendChild(visQ);

            Element pos1 = doc.createElement("Position");
            pos1.setTextContent("Bottom");
            xA.appendChild(pos1);

            Element range1 = doc.createElement("Range");
            xA.appendChild(range1);

            Element fixed = doc.createElement("Fixed");
            range1.appendChild(fixed);

            Element first = doc.createElement("First");
            first.setTextContent(theStartDate);
            fixed.appendChild(first);

            Element last = doc.createElement("Last");
            last.setTextContent(theStopDate);
            fixed.appendChild(last);

            Element subC = doc.createElement("Subchart");
            stdT.appendChild(subC);

            Element weight = doc.createElement("Weight");
            weight.setTextContent("4");
            subC.appendChild(weight);

            Element yAX = doc.createElement("YAxis");
            subC.appendChild(yAX);

            Element vis2 = doc.createElement("Visible");
            vis2.setTextContent("true");
            yAX.appendChild(vis2);

            Element pos2 = doc.createElement("Position");
            pos2.setTextContent("Right");
            yAX.appendChild(pos2);

            Element inv = doc.createElement("Invert");
            yAX.appendChild(inv);
            inv.setTextContent("false");

            Element lO = doc.createElement("Logarithmic");
            yAX.appendChild(lO);
            lO.setTextContent("false");

            Element disP = doc.createElement("Display");
            yAX.appendChild(disP);

            Element mode = doc.createElement("Mode");
            mode.setTextContent("Automatic");
            disP.appendChild(mode);

            Element range2 = doc.createElement("Range");
            yAX.appendChild(range2);

            Element autO = doc.createElement("Automatic");
            range2.appendChild(autO);

            Element analysis1 = doc.createElement("Analysis");
            Element ref = doc.createElement("Reference");
            ref.setTextContent("a0");
            analysis1.appendChild(ref);

            yAX.appendChild(analysis1);

            //SUBCHART1

            if (volumeYN.matches("true")) {

                Element sub1 = doc.createElement("Subchart");
                stdT.appendChild(sub1);

                Element we1 = doc.createElement("Weight");
                we1.setTextContent("2");
                sub1.appendChild(we1);

                Element subYx = doc.createElement("YAxis");
                sub1.appendChild(subYx);

                Element subV = doc.createElement("Visible");
                subV.setTextContent("true");
                subYx.appendChild(subV);

                Element subPos = doc.createElement("Position");
                subPos.setTextContent("Right");
                subYx.appendChild(subPos);

                Element subInv = doc.createElement("Invert");
                subInv.setTextContent("false");
                subYx.appendChild(subInv);

                Element subLog = doc.createElement("Logarithmic");
                subLog.setTextContent("false");
                subYx.appendChild(subLog);

                Element subDisp = doc.createElement("Display");
                subYx.appendChild(subDisp);

                Element subMode = doc.createElement("Mode");
                subMode.setTextContent("Automatic");
                subDisp.appendChild(subMode);

                Element subRange = doc.createElement("Range");
                subYx.appendChild(subRange);

                Element subAuto = doc.createElement("Automatic");
                subRange.appendChild(subAuto);

                Element subAna = doc.createElement("Analysis");
                subYx.appendChild(subAna);

                Element subRef = doc.createElement("Reference");
                subRef.setTextContent("a-1");
                subAna.appendChild(subRef);

                //
            }

            Element scheme = doc.createElement("Scheme");
            chReq.appendChild(scheme);
            Attr xmlnsQ = doc.createAttribute("xmlns");
            xmlnsQ.setValue("http://metastock.com/imageserver/financial");
            scheme.setAttributeNode(xmlnsQ);

            Element bGr = doc.createElement("Background");
            scheme.appendChild(bGr);

            Element bGrM = doc.createElement("BackgroundMode");
            bGrM.setTextContent("Gradient");
            bGr.appendChild(bGrM);

            Element startC = doc.createElement("StartColor");
            bGr.appendChild(startC);

            Element name = doc.createElement("Named");
            name.setTextContent("White");
            startC.appendChild(name);

            Element stopC = doc.createElement("EndColor");
            bGr.appendChild(stopC);

            Element rgb = doc.createElement("RGB");
            rgb.setTextContent("204;204;204");
            stopC.appendChild(rgb);

            Element hatchS = doc.createElement("HatchStyle");
            hatchS.setTextContent("LargeGrid");
            bGr.appendChild(hatchS);

            Element gDr = doc.createElement("GradientMode");
            gDr.setTextContent("ForwardDiagonal");
            bGr.appendChild(gDr);

            Element iMm = doc.createElement("ImageMode");
            iMm.setTextContent("Centered");
            bGr.appendChild(iMm);

            Element border = doc.createElement("Border");
            scheme.appendChild(border);

            Element col = doc.createElement("Color");
            border.appendChild(col);

            Element rgb1 = doc.createElement("RGB");
            rgb1.setTextContent("139;139;155");
            col.appendChild(rgb1);

            Element dashS = doc.createElement("DashStyle");
            border.appendChild(dashS);
            dashS.setTextContent("Solid");

            Element width = doc.createElement("Width");
            width.setTextContent("1");
            border.appendChild(width);

            Element gridL = doc.createElement("GridLines");
            scheme.appendChild(gridL);

            Element color1 = doc.createElement("Color");
            gridL.appendChild(color1);

            Element rgb2 = doc.createElement("RGB");
            rgb2.setTextContent("139;139;155");
            color1.appendChild(rgb2);

            Element dashS1 = doc.createElement("DashStyle");
            dashS1.setTextContent("Dot");
            gridL.appendChild(dashS1);

            Element width2 = doc.createElement("Width");
            width2.setTextContent("1");
            gridL.appendChild(width2);

            Element title1 = doc.createElement("Title");
            scheme.appendChild(title1);

            Element caption2 = doc.createElement("Caption");
            title1.appendChild(caption2);

            Element color2 = doc.createElement("Color");
            caption2.appendChild(color2);

            Element rgb3 = doc.createElement("RGB");
            rgb3.setTextContent("51;0;102");
            color2.appendChild(rgb3);

            Element family = doc.createElement("Family");
            family.setTextContent("Arial");
            caption2.appendChild(family);

            Element style = doc.createElement("Style");
            style.setTextContent("Bold");
            caption2.appendChild(style);

            Element size = doc.createElement("Size");
            size.setTextContent("12");
            caption2.appendChild(size);

            Element range3 = doc.createElement("Range");
            title1.appendChild(range3);

            Element color3 = doc.createElement("Color");
            range3.appendChild(color3);

            Element rgb4 = doc.createElement("RGB");
            rgb4.setTextContent("51;0;102");
            color3.appendChild(rgb4);

            Element family1 = doc.createElement("Family");
            family1.setTextContent("Arial");
            range3.appendChild(family1);

            Element style1 = doc.createElement("Style");
            style1.setTextContent("Regular");
            range3.appendChild(style1);

            Element size1 = doc.createElement("Size");
            size1.setTextContent("8.5");
            range3.appendChild(size1);

            Element legend = doc.createElement("Legend");
            scheme.appendChild(legend);

            Element color = doc.createElement("Color");
            legend.appendChild(color);

            Element rgb5 = doc.createElement("RGB");
            rgb5.setTextContent("51;0;102");
            color.appendChild(rgb5);

            Element family2 = doc.createElement("Family");
            family2.setTextContent("Arial");
            legend.appendChild(family2);

            Element style2 = doc.createElement("Style");
            style2.setTextContent("Regular");
            legend.appendChild(style2);

            Element size2 = doc.createElement("Size");
            size2.setTextContent("8.25");
            legend.appendChild(size2);

            Element xAx = doc.createElement("XAxis");
            scheme.appendChild(xAx);

            Element major = doc.createElement("Major");
            xAx.appendChild(major);

            Element colorB = doc.createElement("Color");
            major.appendChild(colorB);

            Element name1 = doc.createElement("Named");
            name1.setTextContent("Black");
            colorB.appendChild(name1);

            Element family3 = doc.createElement("Family");
            family3.setTextContent("Arial");
            major.appendChild(family3);

            Element style3 = doc.createElement("Style");
            style3.setTextContent("Regular");
            major.appendChild(style3);

            Element size3 = doc.createElement("Size");
            size3.setTextContent("9.75");
            major.appendChild(size3);


            Element minor = doc.createElement("Minor");
            xAx.appendChild(minor);

            Element colorM = doc.createElement("Color");
            minor.appendChild(colorM);

            Element named2 = doc.createElement("Named");
            named2.setTextContent("Black");
            colorM.appendChild(named2);

            Element family4 = doc.createElement("Family");
            family4.setTextContent("Arial");
            minor.appendChild(family4);

            Element style4 = doc.createElement("Style");
            style4.setTextContent("Regular");
            minor.appendChild(style4);

            Element size4 = doc.createElement("Size");
            size4.setTextContent("8.25");
            minor.appendChild(size4);

            Element yAy = doc.createElement("YAxis");
            scheme.appendChild(yAy);

            Element major1 = doc.createElement("Major");
            yAy.appendChild(major1);

            Element colorB1 = doc.createElement("Color");
            major1.appendChild(colorB1);

            Element name1y = doc.createElement("Named");
            name1y.setTextContent("Black");
            colorB1.appendChild(name1y);

            Element family3y = doc.createElement("Family");
            family3y.setTextContent("Arial");
            major1.appendChild(family3y);

            Element style3y = doc.createElement("Style");
            style3y.setTextContent("Regular");
            major1.appendChild(style3y);

            Element size3y = doc.createElement("Size");
            size3y.setTextContent("9.75");
            major1.appendChild(size3y);

            Element minor1 = doc.createElement("Minor");
            yAy.appendChild(minor1);

            Element colorMy = doc.createElement("Color");
            minor1.appendChild(colorMy);

            Element named2y = doc.createElement("Named");
            named2y.setTextContent("Black");
            colorMy.appendChild(named2y);

            Element family4y = doc.createElement("Family");
            family4y.setTextContent("Arial");
            minor1.appendChild(family4y);

            Element style4y = doc.createElement("Style");
            style4y.setTextContent("Regular");
            minor1.appendChild(style4y);

            Element size4y = doc.createElement("Size");
            size4y.setTextContent("8.25");
            minor1.appendChild(size4y);

            Element titleY = doc.createElement("Title");
            yAy.appendChild(titleY);

            Element colorY = doc.createElement("Color");
            titleY.appendChild(colorY);

            Element namedY = doc.createElement("Named");
            namedY.setTextContent("Black");
            colorY.appendChild(namedY);

            Element family5 = doc.createElement("Family");
            family5.setTextContent("Regular");
            titleY.appendChild(family5);

            Element style5 = doc.createElement("Style");
            style5.setTextContent("Regular");
            titleY.appendChild(style5);

            Element size5 = doc.createElement("Size");
            size5.setTextContent("8.25");
            titleY.appendChild(size5);

            Element series = doc.createElement("Series");
            scheme.appendChild(series);

            Element colorS = doc.createElement("Color");
            series.appendChild(colorS);

            Element rgb6 = doc.createElement("RGB");
            rgb6.setTextContent("51;51;255");
            colorS.appendChild(rgb6);

            Element dashSt = doc.createElement("DashStyle");
            dashSt.setTextContent("Solid");
            series.appendChild(dashSt);

            Element width1 = doc.createElement("Width");
            width1.setTextContent("1");
            series.appendChild(width1);

            Element fillC = doc.createElement("FillColor");
            series.appendChild(fillC);

            Element namedF1 = doc.createElement("Named");
            namedF1.setTextContent("Black");
            fillC.appendChild(namedF1);

            Element fillS = doc.createElement("FillStyle");
            fillS.setTextContent("Percent20");
            series.appendChild(fillS);

            Element series1 = doc.createElement("Series");
            scheme.appendChild(series1);

            Element colorS1 = doc.createElement("Color");
            series1.appendChild(colorS1);

            Element rgb9 = doc.createElement("RGB");
            rgb9.setTextContent("51;51;255");
            colorS1.appendChild(rgb9);

            Element dashSt1 = doc.createElement("DashStyle");
            dashSt1.setTextContent("Solid");
            series1.appendChild(dashSt1);

            Element width3= doc.createElement("Width");
            width3.setTextContent("1");
            series1.appendChild(width3);

            Element fillC1 = doc.createElement("FillColor");
            series1.appendChild(fillC1);

            Element namedF = doc.createElement("Named");
            namedF.setTextContent("Black");
            fillC1.appendChild(namedF);

            Element fillS1 = doc.createElement("FillStyle");
            fillS1.setTextContent("Percent20");
            series1.appendChild(fillS1);

            try {
                Source xmlInput = new DOMSource(doc);
                StringWriter stringWriter = new StringWriter();
                StreamResult xmlOutput = new StreamResult(stringWriter);
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                transformerFactory.setAttribute("indent-number", 4);
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(xmlInput, xmlOutput);
                //System.out.println(xmlOutput.getWriter().toString());
                outX = xmlOutput.getWriter().toString();

            } catch (TransformerException e) {
                throw new RuntimeException(e);
            }

        } catch (ParserConfigurationException pe) {
            pe.printStackTrace();
        }

        return outX;
    }
}
