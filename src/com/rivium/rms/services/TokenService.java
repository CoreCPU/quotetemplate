package com.rivium.rms.services;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TokenService {

    private String clTokenFile = "";
    private String clRealTimeTokenFile = "";
    private String clUserId = "";
    private String clAppId = "";
    private String clPassWd = "";
    private String clUrlConnect;
    private String validateTokenFile;

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.services.TokenService");

    public TokenService() {

    }

    public void setClTokenFile(String clTokenFile) {
        this.validateTokenFile = clTokenFile;
        this.clTokenFile = clTokenFile;
    }

    public void setClUserId(String clUserId) {
        this.clUserId = clUserId;
    }

    public void setClAppId(String clAppId) {
        this.clAppId = clAppId;
    }

    public void setClPassWd(String clPassWd) {
        this.clPassWd = clPassWd;
    }

    public String getClUrlConnect() {
        return clUrlConnect;
    }

    public void setClUrlConnect(String clUrlConnect) {

        String secureUrl;

        if (!clUrlConnect.contains("https")) {
            secureUrl = clUrlConnect.replace("http", "https");
        } else {
            secureUrl = clUrlConnect;
        }

        this.clUrlConnect = secureUrl;

        LOGGER.debug("CONNECT : " + this.clUrlConnect);
    }

    public boolean TokenExists (String TokenFile) {

        boolean ItsAlive = false;

        File testFile = new File(TokenFile);

        if (testFile.exists()) {
            LOGGER.debug ("TokenFile exists.");
            ItsAlive = true;
        } else {
            LOGGER.debug("No Tokenfile on disk.");
        }
        return ItsAlive;
    }

    public String getClRealTimeTokenFile() {
        return clRealTimeTokenFile;
    }

    public void setClRealTimeTokenFile(String clRealTimeTokenFile) {
        this.validateTokenFile = clRealTimeTokenFile;
        this.clRealTimeTokenFile = clRealTimeTokenFile;
    }

    public String GetExistingTokenFromFile() {

        String RetToken = "Invalid";
        XMLConfiguration xmlConfig;

        LOGGER.debug ("The Real Time token file path is : " + clTokenFile);

        try {

                if (!clTokenFile.isEmpty()) {
                    try {
                        Configurations config = new Configurations();
                        xmlConfig = config.xml(clTokenFile);
                        if (xmlConfig != null) {
                            RetToken = xmlConfig.getString("Token");
                            LOGGER.debug("Token read from tokenFile : " + RetToken);
                        } else {
                            LOGGER.debug("Problem reading tokenFile location from config file");
                            //RetToken = "INVALID";
                        }
                    } catch (NullPointerException ne){
                        LOGGER.debug("Here is the problem : " + ne.getMessage());
                    }
                } else  {
                    LOGGER.debug("No legal value found for " + clTokenFile);
                }
        } catch (ConfigurationException xmlex) {
            LOGGER.debug("Problem with Token file: " + xmlex.getMessage());
        }
        return RetToken;
    }

    public String GetExistingRealTimeTokenFromFile() {

        String RetToken = "Invalid";
        XMLConfiguration xmlConfig;

        LOGGER.debug ("The Real Time token file path is : " + clRealTimeTokenFile);

        try {

            if (!clRealTimeTokenFile.isEmpty()) {
                try {
                    Configurations config = new Configurations();
                    xmlConfig = config.xml(clRealTimeTokenFile);
                    if (xmlConfig != null) {
                        RetToken = xmlConfig.getString("Real Time Token");
                        LOGGER.debug("Real Time Token read from tokenFile : " + RetToken);
                    } else {
                        LOGGER.debug("Problem reading Real Time tokenFile location from config file");
                        //RetToken = "INVALID";
                    }
                } catch (NullPointerException ne){
                    LOGGER.debug("Here is the problem : " + ne.getMessage());
                }
            } else  {
                LOGGER.debug("No legal value found for " + clRealTimeTokenFile);
            }
        } catch (ConfigurationException xmlex) {
            LOGGER.debug("Problem with Real time Token file: " + xmlex.getMessage());
        }
        return RetToken;
    }

    public String GetTokenFromCloud () {

        InputStream GottenToken;
        String RetToken = "";

        //String secureUrl = clUrlConnect.replace("http", "https");

        LOGGER.debug("Contacting TRKD for new token");

        String XmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<Envelope xmlns=\"http://www.w3.org/2003/05/soap-envelope\">"
                + "<Header>"
                + "<To xmlns=\"http://www.w3.org/2005/08/addressing\">"
                + clUrlConnect
                + "/api/2006/05/01/TokenManagement_1.svc/Anonymous</To>"
                + "<MessageID xmlns=\"http://www.w3.org/2005/08/addressing\">java_simple_"
                + new Date().getTime()
                + "</MessageID>"
                + "<Action xmlns=\"http://www.w3.org/2005/08/addressing\">http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1/CreateServiceToken_1</Action>"
                + "</Header>"
                + "<Body>"
                + "<CreateServiceToken_Request_1 xmlns:global=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1\" xmlns=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1\">"
                + "<global:ApplicationID>"
                + clAppId
                + "</global:ApplicationID>"
                + "<Username>"
                + clUserId
                + "</Username>"
                + "<Password>"
                + clPassWd
                + "</Password>"
                + "</CreateServiceToken_Request_1>"
                + "</Body>"
                + "</Envelope>";

        LOGGER.debug("The Token Request: " + XmlString);

        try {

            LOGGER.debug("Performing token request to TRKD");

            GottenToken = sendRequest(clUrlConnect + "/api/2006/05/01/TokenManagement_1.svc/Anonymous", XmlString);

            RetToken = ParseTokenFromCloud(GottenToken);

            LOGGER.debug("The granted token retrieved from TRKD : " + RetToken);

        } catch (IOException e) {
            LOGGER.debug ("Problem with getting Token: " + e.getMessage());
            System.err.println("Error : " + e.getMessage());
        } catch (Exception e) {
            LOGGER.debug ("Problem with getting Token: " + e.getMessage());
            System.err.println("TokenError: " + e.getMessage());
        }

        LOGGER.debug ("Offering this token : " + RetToken);

        return RetToken;

    }

    private static InputStream sendRequest(String urlString, String body) throws IOException {

        LOGGER.debug ("Connecting to " + urlString);

        URL url = new URL(urlString);

        String loseHttp = urlString.substring(7);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", loseHttp);
        connection.setRequestProperty("Content-Type", "application/soap+xml");
        connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

        OutputStreamWriter os = new OutputStreamWriter(new BufferedOutputStream(connection.getOutputStream()));

        os.write(body);
        os.flush();

        return new BufferedInputStream(connection.getInputStream());
    }

    public String ValidateToken (String passedToken) {

        InputStream GottenToken;
        String ValidatedToken = "";
        XMLConfiguration xmlConfig;

        //LOGGER.debug("Getting ready to contact TRKD to validate Token.");
        LOGGER.debug ("Validating token timestamp.");

        Configurations cfg = new Configurations();

        try {
            xmlConfig = cfg.xml(validateTokenFile);
            String xmlDate = xmlConfig.getString("Expiration");
            LOGGER.debug ("Token Expiration Date : " + xmlDate);

            LocalDateTime ltime = new LocalDateTime();
            DateTime localTimeInUTC = new DateTime(ltime.toString(), DateTimeZone.UTC);

            DateTime expireTime = new DateTime(xmlDate, DateTimeZone.UTC);

            if (localTimeInUTC.isAfter(expireTime.plusMinutes(75))) {
                ValidatedToken = "Invalid";
                LOGGER.debug ("Token is invalid. Need to get new token.");
            } else {
                ValidatedToken = "Valid";
                LOGGER.debug ("Token is valid. Using cached token.");
            }

        } catch (ConfigurationException ce) {
            LOGGER.debug("Configuration Error Message : " + ce.getMessage());

        }

/*        //String secureUrl = clUrlConnect.replace("http", "https");

        String XmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<Envelope xmlns=\"http://www.w3.org/2003/05/soap-envelope\">"
                + "<Header>"
                + "<To xmlns=\"http://www.w3.org/2005/08/addressing\">"
                + clUrlConnect
                + "/api/TokenManagement/TokenManagement.svc</To>"
                + "<MessageID xmlns=\"http://www.w3.org/2005/08/addressing\">java_simple_"
                + new Date().getTime()
                + "</MessageID>"
                + "<Action xmlns=\"http://www.w3.org/2005/08/addressing\">http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1/ValidateToken_1</Action>"
                + "<Authorization xmlns=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1\">"
                + "<ApplicationID>"
                + clAppId
                + "</ApplicationID>"
                + "<Token>"
                + passedToken
                + "</Token>"
                + "</Authorization>"
                + "</Header>"
                + "<Body>"
                + "<ValidateToken_Request_1 xmlns=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/TokenManagement_1\" xmlns:global=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1\">"
                + "<ApplicationID xmlns=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1\">"
                + clAppId
                + "</ApplicationID>"
                + "<Token xmlns=\"http://www.reuters.com/ns/2006/05/01/webservices/rkd/Common_1\">"
                + passedToken
                + "</Token>"
                + "</ValidateToken_Request_1>"
                + "</Body>"
                + "</Envelope>";

        try {
            LOGGER.debug("Performing validation of Token: " + XmlString);
            GottenToken = sendRequest(clUrlConnect + "/api/TokenManagement/TokenManagement.svc", XmlString);
            ValidatedToken = ParseValidation(GottenToken);
            LOGGER.debug("Validated Token :" + ValidatedToken);
        }
        catch (IOException e) {
            ValidatedToken = "Invalid";
            LOGGER.debug("Problem validating Token:" + e.getMessage());
            System.err.println("Val_Error : " + e.getMessage());
        }
        catch (Exception e) {
            ValidatedToken = "Invalid";
            LOGGER.debug("Problem validating Token:" + e.getMessage());
            System.err.println("Val_TokenError: " + e.getMessage());
        }*/

        LOGGER.debug("Returned validated Token.");
        return ValidatedToken;
    }

    private String ParseValidation(InputStream StreamValidation) {

        String Validated = "";

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(StreamValidation);

            StreamValidation.close();

            document.getElementsByTagName("Valid");

            Validated = document.getElementsByTagName("Valid").item(0).getTextContent();
        }
        catch (IOException e) {
            LOGGER.debug ("Validate Error : " + e.getMessage());
            //tokenmanager.debug("Request Failed. IO-Reason :" + e.getMessage());
        }
        catch (Exception e) {
            LOGGER.debug("Validate_TokenError: " + e.getMessage());
            //tokenmanager.debug("Token Request Failed. Reason :" + e.getMessage());
        }

        return Validated;
    }

//    private static StringBuffer getResponseStringBuffer(InputStream is) throws IOException
//    {
//        StringBuffer retval = new StringBuffer();
//
//        int bytesRead = 0;
//        byte[] buffer = new byte[500*1024];
//
//        while ((bytesRead = is.read(buffer)) != -1) {
//            retval.append(new String(buffer, 0, bytesRead));
//        }
//
//        return retval;
//    }


    private String ParseTokenFromCloud (InputStream StreamToken){

        String ReturnToken = "";


        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(StreamToken);

            StreamToken.close();

            ReturnToken = document.getElementsByTagName("global:Token").item(0).getTextContent();
            LOGGER.debug ("The token from TRKD : " + ReturnToken);
            String retDate = document.getElementsByTagName("Expiration").item(0).getTextContent();
            LOGGER.debug ("Expiration date : " + retDate);
            WriteTokenToFile(ReturnToken, retDate, clTokenFile);

        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
            LOGGER.debug("Request Failed. Reason : " + e.getMessage());
            LOGGER.debug ("Token not saved to file.");

        } catch (Exception e) {
            System.err.println("TokenError: " + e.getMessage());
            LOGGER.debug("Request Failed. Reason : " + e.getMessage());
            LOGGER.debug ("Token not saved to file.");
        }

        return ReturnToken;

    }

    private void WriteTokenToFile (String wToken, String wExpiration, String wFile) {

        //boolean itswritten;

        LOGGER.debug("The Token to store in TokenFile : " + wToken);

        try {
            DocumentBuilderFactory Builder = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = Builder.newDocumentBuilder();

            Document xmldoc = docBuilder.newDocument();
            Element rootElement = xmldoc.createElement("TrkdSettings");
            xmldoc.appendChild(rootElement);

            Element TheToken = xmldoc.createElement("Token");
            TheToken.appendChild(xmldoc.createTextNode(wToken));
            rootElement.appendChild(TheToken);

            Element TheExpiration = xmldoc.createElement("Expiration");
            TheExpiration.appendChild(xmldoc.createTextNode(wExpiration));
            rootElement.appendChild(TheExpiration);

            TransformerFactory transformer = TransformerFactory.newInstance();
            Transformer TransForm = transformer.newTransformer();
            DOMSource domsource = new DOMSource(xmldoc);

            StreamResult result = new StreamResult(new File(wFile));

            TransForm.setOutputProperty("indent", "yes");
            TransForm.setOutputProperty(OutputKeys.INDENT, "yes");
            TransForm.transform(domsource, result);

            //itswritten = true;
            LOGGER.debug ("Token stored in tokenFile");

        } catch (TransformerException | ParserConfigurationException tex) {
            //itswritten = false;
            LOGGER.debug("Error writing TokenFile: " + tex.getMessage());
        }

        //return itswritten;
    }
}
