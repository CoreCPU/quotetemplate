package com.rivium.rms;/*
 * Created by alain on 2/20/16.
 */

import com.rivium.rms.services.HeadLinesService;
import com.rivium.rms.services.TokenService;
import com.rivium.rms.services.ServerService;
import com.rivium.rms.types.TrkdNewsItem;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.Attr;

public class NewsQuery extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger("com.rivium.rms.NewsQuery");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        XMLConfiguration xmlConfig;
        String xmlPassWd = "";
        String xmlAppId = "";
        String xmlUserId = "";
        String tokenFilePath;
        String realTimeTokenFilePath;
        String passTokenFilePath = "";
        String runConfig = getServletConfig().getInitParameter("ConfigFile");
        String dataRep;
        String QueryStr;
        String userId;
        String xmlUrlConnect;
        String xmlRTPassWd = "";
        String xmlRTAppId = "";
        String xmlRTUserId = "";
        String xmlConnectAddress = "";
        String[] xmlConnectAddresses = null;
        ArrayList<String> delUids = new ArrayList<>();
        ArrayList<String> realUids = new ArrayList<>();
        List<Object> delayedUids;
        List<Object> realtimeUids;
        List<Object> urlsInConfig;

        LOGGER.info("\n++++++New headlines request++++++\n");

        request.setCharacterEncoding("UTF-8");
        PrintWriter pw = response.getWriter();
        response.setContentType("text/xml;charset=UTF-8");

        try {
            if (runConfig.isEmpty()) {
                LOGGER.debug("FATAL ERROR : Couldn't read configuration file.");
                LOGGER.info ("No configuration file found.");
                return;
            } else {
                LOGGER.info("RunConfig : " + runConfig);

                Configurations config = new Configurations();

                try {
                    xmlConfig = config.xml(runConfig);

                    if (xmlConfig.getString("trkd.timelines.delayed").matches("true")) {
                        xmlPassWd = xmlConfig.getString("trkd.delayed.passWd");
                        if (checkString(xmlPassWd)) {
                            LOGGER.info ("Issue with provided dalayed configuration values: password");
                            LOGGER.debug("FATAL ERROR : NO DELAYED PASSWD GIVEN");
                            return;
                        }

                        xmlAppId = xmlConfig.getString("trkd.delayed.applicationId");
                        if (checkString(xmlAppId)) {
                            LOGGER.info ("Issue with provided dalayed configuration values: applicationId");
                            LOGGER.debug("FATAL ERROR : NO DELAYED APPID GIVEN");
                            return;
                        }

                        xmlUserId = xmlConfig.getString("trkd.delayed.userId");
                        if (checkString(xmlUserId)) {
                            LOGGER.info ("Issue with provided dalayed configuration values: uid");
                            LOGGER.debug("FATAL ERROR : NO DELAYED USERID GIVEN");
                            return;
                        }
                    }

                    if (xmlConfig.getString("trkd.timelines.realtime").matches("true")) {
                        xmlRTPassWd = xmlConfig.getString("trkd.realtime.passWd");
                        if (checkString(xmlRTPassWd)) {
                            LOGGER.info ("Issue with provided realtime configuration values: password");
                            LOGGER.debug("FATAL ERROR : NO REALTIME PASSWD GIVEN");
                            return;
                        }

                        xmlRTAppId = xmlConfig.getString("trkd.realtime.applicationId");
                        if (checkString(xmlRTAppId)) {
                            LOGGER.info ("Issue with provided realtime configuration values: applicationId");
                            LOGGER.debug("FATAL ERROR : NO REALTIME APPID GIVEN");
                            return;
                        }

                        xmlRTUserId = xmlConfig.getString("trkd.realtime.userId");
                        if (checkString(xmlRTUserId)) {
                            LOGGER.info ("Issue with provided realtime configuration values: uid");
                            LOGGER.debug("FATAL ERROR : NO REALIME USERID GIVEN");
                            return;
                        }

                        urlsInConfig = xmlConfig.getList( "trkd.connect.servers.server");
                        if (urlsInConfig.isEmpty()) {
                            LOGGER.info ("Issue with provided servers in config");
                            LOGGER.debug("FATAL ERROR : CAN NOT CONNECT");
                            return;
                        } else {
                            xmlConnectAddresses = urlsInConfig.toArray(new String[0]);
                            for (String t : xmlConnectAddresses) {
                                LOGGER.debug("LIST CONTENT : " + t);
                            }
                            ServerService theServer = new ServerService();
                            theServer.setServerList(xmlConnectAddresses);
                            xmlConnectAddress = theServer.getServerName();
                            LOGGER.debug ("THIS IS THE SERVERNAME : " + xmlConnectAddress);
                        }
                    }

                    tokenFilePath = xmlConfig.getString("common.tokenfile");

                    if (checkString(tokenFilePath)) {
                        LOGGER.info("No path to delayed tokenfile");
                        LOGGER.debug ("FATAL ERROR : NO TOKENFILE PATH GIVEN");
                        return;
                    }

                    realTimeTokenFilePath = xmlConfig.getString("common.RTtokenfile");

                    if (checkString(realTimeTokenFilePath)) {
                        LOGGER.info("No path to realtime tokenfile");
                        LOGGER.debug ("FATAL ERROR : NO REALTIME TOKENFILE PATH GIVEN");
                        return;
                    }

                    delayedUids = xmlConfig.getList("common.users.delayed.uid");
                    if (delayedUids.isEmpty()) {
                        LOGGER.debug ("No delayed user list defined.");
                        return;
                    } else {
                        //delUids = delayedUids.toArray(new String[delayedUids.size()]);
                        for (Object q : delayedUids){
                            String d = q.toString();
                            delUids.add(d);
                        }
                    }

                    realtimeUids = xmlConfig.getList("common.users.realtime.uid");
                    if (realtimeUids.isEmpty()) {
                        LOGGER.debug ("No realtime user list defined.");
                        return;
                    } else {
                        //realUids = realtimeUids.toArray(new String[realtimeUids.size()]);
                        realUids.addAll(realtimeUids.stream().map(Object::toString).collect(Collectors.toList()));
                    }

                } catch (ConfigurationException ce) {
                    LOGGER.debug ("Configuration Error Message : " + ce.getMessage());
                    return;
                }
            }
        } catch (NullPointerException ne) {
            LOGGER.info ("Configuration file issues. Was it specified in web.xml ?");
            LOGGER.debug ("FATAL ERROR : Configuration not specified in web.xml");
            return;
        }

        dataRep = request.getParameter("DataRepresentation");
        QueryStr = request.getParameter("QueryStr");
        userId = request.getParameter("UserId");

        try {
            if (userId.matches("")) {
                userId = "sds";
            }
        } catch (NullPointerException ne) {
            userId = "sds";
        }

        if (delayedUids.contains(userId)) {
            passTokenFilePath = tokenFilePath;
        }

        if (realtimeUids.contains(userId)) {
            passTokenFilePath = realTimeTokenFilePath;
        }

        try {
            if (QueryStr.isEmpty()) {
                LOGGER.debug("FATAL ERROR : No Query String provided. Unable to search for headlines.");
                //System.err.println("FATAL ERROR : No symbol given in request string. Unable to fetch data.");
                return;
            } else {
                LOGGER.debug("DataRepresentation : " + dataRep + "\\t" + "Query String : " + QueryStr + "\\t" + "UserId : " + userId);
                //System.err.println("DataRepresentation : " + dataRep + "\\t" + "Symbol : " + symBol + "\\t" + "UserId : " + userId);
            }
        } catch (NullPointerException nee) {
            LOGGER.debug ("No QueryString provided. Likely reason is incorrectly formatted request.");
            //System.err.println("No value found for Symbol. Likely reason is incorrectly formatted request.");
            return;
        }

        LOGGER.debug ("Calling TokenService");

        TokenService tokenService = new TokenService();

        //if (userId.matches("sds")) {
        if (delUids.contains(userId)) {
            tokenService.setClAppId(xmlAppId);
            tokenService.setClUserId(xmlUserId);
            tokenService.setClPassWd(xmlPassWd);
            passTokenFilePath = tokenFilePath;
            LOGGER.debug ("Delayed User Id : " + userId);
            LOGGER.debug ("DELAYED USER :" + xmlUserId);
        } //else if (userId.matches("sds2"))
        else if (realUids.contains(userId)) {
            tokenService.setClAppId(xmlRTAppId);
            tokenService.setClUserId(xmlRTUserId);
            tokenService.setClPassWd(xmlRTPassWd);
            passTokenFilePath = realTimeTokenFilePath;
            LOGGER.debug ("Real time User Id : " + userId);
            LOGGER.debug("RT-USER : " + xmlRTUserId);
        }


        tokenService.setClTokenFile(passTokenFilePath);
        tokenService.setClUrlConnect(xmlConnectAddress);

        File savedToken = new File(passTokenFilePath);
        String aToken;

        if (savedToken.exists()) {
            aToken = tokenService.GetExistingTokenFromFile();
            LOGGER.debug("Gotten token from file");
            if (!aToken.equals("Invalid")) {
                LOGGER.debug ("Checking validity of existing token");
                String validateT = tokenService.ValidateToken(aToken);
                if (validateT.equals("Invalid")) {
                    LOGGER.debug("Token is " + validateT + " Requesting new token from cloud.");
                    aToken = tokenService.GetTokenFromCloud();
                    LOGGER.debug("Gotten token from cloud : " + aToken);
                }
            } else {
                LOGGER.debug ("Problem reading token from tokenFile. Requesting new token.");
                aToken = tokenService.GetTokenFromCloud();
                LOGGER.debug("Gotten token from cloud : " + aToken);

            }
        } else {
            LOGGER.debug("No token file found. Requesting new token from cloud.");
            aToken = tokenService.GetTokenFromCloud();
            LOGGER.debug("Gotten token from cloud : " + aToken);
        }

        if (aToken.isEmpty()) {
            LOGGER.debug ("No token received from authentication server. Can not complete request.");
            return;
        }

        LOGGER.info ("Calling HeadLinesService");

        HeadLinesService HlService = new HeadLinesService();
        String delim = ":";
        String[] Splitter;
        LOGGER.info ("Requesting headlines for " + QueryStr);
        Splitter = QueryStr.split(delim);
        HlService.setHlQueryString(Splitter[0]);
        LOGGER.debug("Setting token to" + aToken);
        LOGGER.debug ("Setting query string to " + Splitter[0]);
        HlService.setHlToken(aToken);

        if (delUids.contains(userId)) {
            HlService.setHlAppId(xmlAppId);
        } else if (realUids.contains(userId)) {
            HlService.setHlAppId(xmlRTAppId);
        }

        HlService.setConnectAddress(xmlConnectAddress);

        ArrayList<TrkdNewsItem> headLs = HlService.GetTheHeadLines();

        LOGGER.info ("Headlines request complete.");

        pw.append(makeXml(headLs));
        pw.flush();
        pw.close();

    }

    private boolean checkString (String checker) {

        Boolean retVal = false;

        try {
            if (checker.isEmpty()) {
                LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
                retVal = true;
            } else {
                retVal = false;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            LOGGER.debug("Setting " + checker + " has no value. Check entries in configuration file.");
        }

        return retVal;
    }

    private String makeXml (ArrayList<TrkdNewsItem> gottenHeadLines) {

        String retXml = "";

        Document doc;

        LOGGER.debug("Building XML for sending retrieved headlines back to client");

        try {
            DocumentBuilderFactory docF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docB = docF.newDocumentBuilder();

            doc = docB.newDocument();
            Element rootEl = doc.createElement("HEADLINEML");
            doc.appendChild(rootEl);

            for (TrkdNewsItem headL : gottenHeadLines) {
                Element hl = doc.createElement("HL");
                Attr xmlspace = doc.createAttribute("xml:space");
                xmlspace.setValue("preserve");
                hl.setAttributeNode(xmlspace);
                rootEl.appendChild(hl);
                Element id = doc.createElement("ID");
                id.setTextContent(headL.getID());
                hl.appendChild(id);
                Element re = doc.createElement("RE");
                re.setTextContent(headL.getRE());
                hl.appendChild(re);
                Element st = doc.createElement("ST");
                st.setTextContent(headL.getST());
                hl.appendChild(st);
                Element ct = doc.createElement("CT");
                ct.setTextContent(headL.getCT());
                hl.appendChild(ct);
                Element rt = doc.createElement("RT");
                rt.setTextContent(headL.getRT());
                hl.appendChild(rt);
                Element pr = doc.createElement("PR");
                pr.setTextContent(headL.getPR());
                hl.appendChild(pr);
                Element at = doc.createElement("AT");
                at.setTextContent(headL.getAT());
                hl.appendChild(at);
                Element ur = doc.createElement("UR");
                ur.setTextContent(headL.getUR());
                hl.appendChild(ur);
                Element ln = doc.createElement("LN");
                ln.setTextContent(headL.getLN());
                hl.appendChild(ln);
                Element ht = doc.createElement("HT");
                ht.setTextContent(headL.getHT());
                hl.appendChild(ht);
                LOGGER.debug ("Headlines found : " + headL.getHT());
                Element pe = doc.createElement("PE");
                pe.setTextContent(headL.getPE());
                hl.appendChild(pe);
                Element co = doc.createElement("CO");
                co.setTextContent(headL.getCO());
                hl.appendChild(co);
                Element tn = doc.createElement("TN");
                tn.setTextContent(headL.getTN());
                hl.appendChild(tn);
            }

            TransformerFactory tFf = TransformerFactory.newInstance();
            Transformer tF = tFf.newTransformer();
            tF.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domS = new DOMSource(doc);
            StreamResult strR = new StreamResult(new StringWriter());

            tF.transform(domS, strR);
            retXml = strR.getWriter().toString();
            LOGGER.debug ("Received data transformed for sending to client.");

        } catch (ParserConfigurationException | TransformerException pe) {
            pe.printStackTrace();
        }

        return retXml;
    }
}
