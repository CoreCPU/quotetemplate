package com.rivium.rms.types;/*
 * Created by alain on 2/20/16.
 */

class TrkdTopNewsItem {

    private String TopRicDate;
    private String TopRicLine;
    private String TopRicLink;

    public String getTopRicDate() {
        return TopRicDate;
    }

    public void setTopRicDate(String topRicDate) {
        TopRicDate = topRicDate;
    }

    public String getTopRicLine() {
        return TopRicLine;
    }

    public void setTopRicLine(String topRicLine) {
        TopRicLine = topRicLine;
    }

    public String getTopRicLink() {
        return TopRicLink;
    }

    public void setTopRicLink(String topRicLink) {
        TopRicLink = topRicLink;
    }
}
