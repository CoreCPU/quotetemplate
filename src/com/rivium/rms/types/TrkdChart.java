package com.rivium.rms.types;

public class TrkdChart {

    private String server;
    private String imageTag;
    private String httpUrl;
    private String httpsUrl;
    private String mplsHttpUrl;
    private String mplsHttpsUrl;

    public TrkdChart(){

    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getImageTag() {
        return imageTag;
    }

    public void setImageTag(String imageTag) {
        this.imageTag = imageTag;
    }

    public String getHttpUrl() {
        return httpUrl;
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

    public String getHttpsUrl() {
        return httpsUrl;
    }

    public void setHttpsUrl(String httpsUrl) {
        this.httpsUrl = httpsUrl;
    }

    public String getMplsHttpUrl() {
        return mplsHttpUrl;
    }

    public void setMplsHttpUrl(String mplsHttpUrl) {
        this.mplsHttpUrl = mplsHttpUrl;
    }

    public String getMplsHttpsUrl() {
        return mplsHttpsUrl;
    }

    public void setMplsHttpsUrl(String mplsHttpsUrl) {
        this.mplsHttpsUrl = mplsHttpsUrl;
    }
}