package com.rivium.rms.types;

import java.util.ArrayList;


public class TrkdQuote {

    private String ricName;
    private ArrayList<TrkdField> fieldList = new ArrayList<>();
    private String ricStatus;
    private String ricType;
    private String QoS;

    public TrkdQuote() {

    }

    public String getRicName() {
        return ricName;
    }

    public void setRicName(String ricName) {
        this.ricName = ricName;
    }

    public ArrayList<TrkdField> getFieldList() {
        return fieldList;
    }

    public void setFieldList(ArrayList<TrkdField> fieldList) {
        this.fieldList = fieldList;
    }

    public String getRicStatus() {
        return ricStatus;
    }

    public void setRicStatus(String ricStatus) {
        this.ricStatus = ricStatus;
    }

    public String getFieldValueByName(String fieldName) {

        String retVal = "";

        for (TrkdField tField : fieldList) {
            if (tField.getFieldName().equals(fieldName)) {
                retVal = tField.getFieldValue();
                break;
            }
        }

        return retVal;
    }

    public String getRicType() {
        return ricType;
    }

    public void setRicType(String ricType) {
        this.ricType = ricType;
    }

    public String getQoS() {
        return QoS;
    }

    public void setQoS(String qoS) {
        QoS = qoS;
    }

}